#!/usr/bin/env python
#-*-coding:utf8;-*-
import random,sys
pot_tam = 3 #2-9, 3-27, 4-81, 5-243, 6-729, 7-2187
lista = sorted(random.sample(range(0,3**(pot_tam+1)),3**pot_tam))

def buscar(lst,num,pos=0):
 print(lst)
 mid=int((len(lst)/2))
 if(len(lst)==1 and lst[0]!=num): return None
 if(lst[mid]>num):
  return buscar(lst[0:mid],num,pos)
 if(lst[mid]<num):
  pos+=mid
  return buscar(lst[mid:len(lst)],num,pos)
 if(lst[mid]==num): return pos+mid+1

pos=buscar(lista,int(sys.argv[1]))  
print("POSICIÓN: ",pos,"(",lista[pos-1],") entre ",3**pot_tam," números",sep="") if(pos) else print("No está el número en la lista")