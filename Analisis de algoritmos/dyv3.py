#!/usr/bin/env python
#-*-coding:utf8;-*-
import random, sys	#rango de num, rango a tomar
pot_tam = 3	#2-9, 3-27, 4-81, 5-243, 6-729, 7-2187
#rlist = sorted(random.sample(range(0,3**(pot_tam+1)),3**pot_tam))
rlist = [1]
def buscar(lst,num,pos=0):
	print(lst)
	if(len(lst)>=3):
		a=len(lst)//3     #1/3
		b=a*2 #2/3
		if(lst[a]==num):
			pos+=a
			return pos+1
		if(lst[b]==num):
			pos+=b
			return pos+1
		if(num<lst[a]):	#primer tercio
			pos=pos+0
			return buscar(lst[0:a],num,pos)
		if(num>lst[a] and num<lst[b]):
			pos+=a 		#segundo tercio
			return buscar(lst[a:b],num,pos)
		if(num>lst[b]):	#último tercio
			pos+=b
			return buscar(lst[b:len(lst)],num,pos)
	if(len(lst)==1 and lst[0]==num):	return pos+1
	else:	return None
	
pos=buscar(rlist,int(sys.argv[1]))
print("POSICIÓN: ",pos,"(",rlist[pos-1],") entre ",3**pot_tam," números",sep="") if(pos) else print("No está el número en la lista")