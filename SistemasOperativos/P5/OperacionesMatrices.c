#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <sys/types.h>
#include "operadores.h"
#define SIZ_MAX 3
pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;
void *hilosuma(void *data);
void *hiloresta(void *data);
void *hilomult(void *data);
void *hilotransp(void *data);
void *hiloinv(void *data);
void *hiloLector(void *data);
struct datos {
    int iii; //Estructura para pasar datos entre hilos.
}; //Estructuras para pasar datos entre proceso e hilos
int main() //void ImpMat(float matrix[SIZ_MAX][SIZ_MAX], int row, int column, char* nombre, int nodecim, int nofile, int nomostrar);
{   //void CargarEnArreglo(char *NombreArchivo, float matrix[SIZ_MAX][SIZ_MAX],int tam, int skiprows);
    struct datos *datos = malloc(sizeof(struct datos));
    printf("PID PADRE: %d\n",getpid());
    float a[SIZ_MAX][SIZ_MAX],b[SIZ_MAX][SIZ_MAX]; int contc=1;
    for(int i=0;i<SIZ_MAX;i++)      //debido a que es dificil crear de manera
                for(int j=0;j<SIZ_MAX;j++)  //automatizada una matriz 10x10
                {                       //donde todas las entradas sean
                    a[i][j]=contc;      //Lineal.Independientes (para que exista una inversa)
                    b[i][j]=contc;      //DEMO
                    contc++;            //DEMO
                }                       //DEMO
            a[2][1]=b[2][1]=9;          //<= se hacen cambios en ciertos indices
            a[2][2]=b[2][2]=8;          //<= para tener una matriz L.I 
    /*printf("Inserte Matriz A: \n"); //IngresaMatrizA
    InsEnMat(a,SIZ_MAX,SIZ_MAX);    //Se ordena guardar matriz en un archivo, sin imprimirla a pantalla
    ImpMat(a,SIZ_MAX,SIZ_MAX,"OriginalA",1,0,1);
    printf("Inserte Matriz B: \n"); 
    InsEnMat(b,SIZ_MAX,SIZ_MAX);    
    ImpMat(b,SIZ_MAX,SIZ_MAX,"OriginalB",1,0,1);*/
    pthread_t thread[6]; //Handlers de los primeros 5 hilos
    int hilonum[6];
    hilonum[0]=pthread_create(&(thread[0]),NULL, hilosuma, datos);
        pthread_join(thread[0], NULL); //Espera a que todos los hilos acaben
    hilonum[1]=pthread_create(&(thread[1]),NULL, hiloresta, datos);
        pthread_join(thread[1], NULL); //Espera a que todos los hilos acaben
    hilonum[2]=pthread_create(&(thread[2]),NULL, hilomult, datos);
        pthread_join(thread[2], NULL); //Espera a que todos los hilos acaben    
    hilonum[3]=pthread_create(&(thread[3]),NULL, hilotransp, datos);
        pthread_join(thread[3], NULL); //Espera a que todos los hilos acaben    
    hilonum[4]=pthread_create(&(thread[4]),NULL, hiloinv, datos);
        pthread_join(thread[4], NULL); //Espera a que todos los hilos acaben    
    hilonum[5]=pthread_create(&(thread[5]),NULL, hiloLector, datos);
        pthread_join(thread[5], NULL); //Espera a que todos los hilos acaben    
    for(int i=0;i<6;i++)
        if (hilonum[i])
            printf("No fue posible crear un hilo.\n");
    exit(0);
    }

void *hilosuma(void *data)
{
    struct datos *datos = data;
    printf("\n===SUMA=== [HILO | ThreadID: %d PID: %d]\n",ObtenerIDHilo(),getpid());
    float a[SIZ_MAX][SIZ_MAX],b[SIZ_MAX][SIZ_MAX],c[SIZ_MAX][SIZ_MAX];
    CargarEnArreglo("OriginalA",a,SIZ_MAX,1);
    CargarEnArreglo("OriginalB",b,SIZ_MAX,1);
    SumaOResta(a,b,c,SIZ_MAX,SIZ_MAX,1); //solo si es 1 las va sumar
    ImpMat(c,SIZ_MAX,SIZ_MAX,"suma",1,0,0);
    pthread_exit(NULL); //Sale del hilo
}
void *hiloresta(void *data)
{
    struct datos *datos = data;
    printf("\n===RESTA=== [HILO | ThreadID: %d PID: %d]\n",ObtenerIDHilo(),getpid());
    float a[SIZ_MAX][SIZ_MAX],b[SIZ_MAX][SIZ_MAX],c[SIZ_MAX][SIZ_MAX];
    CargarEnArreglo("OriginalA",a,SIZ_MAX,1);
    CargarEnArreglo("OriginalB",b,SIZ_MAX,1);
    SumaOResta(a,b,c,SIZ_MAX,SIZ_MAX,0); //solo si es 1 las va sumar
    ImpMat(c,SIZ_MAX,SIZ_MAX,"resta",1,0,0);
    printf("\n");
    pthread_exit(NULL); //Sale del hilo
}
void *hilomult(void *data)
{
    struct datos *datos = data;
    printf("\n===MULTIPLICACION=== [HILO | ThreadID: %d PID: %d]\n",ObtenerIDHilo(),getpid());
    float a[SIZ_MAX][SIZ_MAX],b[SIZ_MAX][SIZ_MAX],c[SIZ_MAX][SIZ_MAX];
    CargarEnArreglo("OriginalA",a,SIZ_MAX,1);
    CargarEnArreglo("OriginalB",b,SIZ_MAX,1);
    Multiplicar(a,b,c); 
    ImpMat(c,SIZ_MAX,SIZ_MAX,"multiplicacion",1,0,0);
    printf("\n");
    pthread_exit(NULL); //Sale del hilo
}
void *hilotransp(void *data)
{
    struct datos *datos = data;
    printf("\n===TRANSPUESTAS=== [HILO | ThreadID: %d PID: %d]\n",ObtenerIDHilo(),getpid());
    float a[SIZ_MAX][SIZ_MAX],b[SIZ_MAX][SIZ_MAX],c[SIZ_MAX][SIZ_MAX];
    CargarEnArreglo("OriginalA",a,SIZ_MAX,1);
    CargarEnArreglo("OriginalB",b,SIZ_MAX,1);
    Transpuesta(a,c,SIZ_MAX,SIZ_MAX);
    printf("Transpuesta de A:\n");
    ImpMat(c,SIZ_MAX,SIZ_MAX,"TranspuestaA",1,0,0);
    Transpuesta(b,c,SIZ_MAX,SIZ_MAX);
    printf("Transpuesta de B:\n");
    ImpMat(c,SIZ_MAX,SIZ_MAX,"TranspuestaB",1,0,0);
    printf("\n");
    pthread_exit(NULL); //Sale del hilo
}
void *hiloinv(void *data)
{
    struct datos *datos = data;
    printf("\n===INVERSAS=== [HILO | ThreadID: %d PID: %d]\n",ObtenerIDHilo(),getpid());
    float a[SIZ_MAX][SIZ_MAX],b[SIZ_MAX][SIZ_MAX],c[SIZ_MAX][SIZ_MAX];
    CargarEnArreglo("OriginalA",a,SIZ_MAX,1);
    CargarEnArreglo("OriginalB",b,SIZ_MAX,1);
    if(determ(a,SIZ_MAX)!=0 && determ(b,SIZ_MAX)!=0){
       TreInversaCaller(a,c,SIZ_MAX);
       printf("\nInversa Matriz A:\n");
       ImpMat(c,SIZ_MAX,SIZ_MAX,"InversaA",0,0,0);
       TreInversaCaller(b,c,SIZ_MAX);
       printf("\nInversa Matriz B:\n");
       ImpMat(c,SIZ_MAX,SIZ_MAX,"InversaB",0,0,0);
       }
    printf("\n");
    pthread_exit(NULL); //Sale del hilo
}
void *hiloLector(void *data)
{
    struct datos *datos = data;
    printf("\n===LECTURA ARCHIVOS CREADOS=== [HILO | ThreadID: %d PID: %d]\n",ObtenerIDHilo(),getpid());
    LeerArchivos("OriginalA");
    LeerArchivos("OriginalB");
    LeerArchivos("suma");
    LeerArchivos("resta");
    LeerArchivos("multiplicacion");
    LeerArchivos("TranspuestaA");
    LeerArchivos("TranspuestaB");
    LeerArchivos("InversaA");
    LeerArchivos("InversaB");    
    pthread_exit(NULL); //Sale del hilo
}
