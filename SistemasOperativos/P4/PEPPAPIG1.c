#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#define max_cant_pids 666
#define hijos_dsp_mainCh 10
void niceFunc(int s, int m);
int main()
{
	int i,pids[max_cant_pids],n=hijos_dsp_mainCh,pidCh;
	pidCh=fork(); //Se crea el hijo principal
	if(pidCh==0)
	{
		for (i = 0; i < n; ++i) {
  		if ((pids[i]=fork())<0) 
    		printf("Error al crear hijo.");
  		else if (pids[i] == 0) {
  			if(i==0)
		  		niceFunc(i,5); //niceFunc recibe un entero mayor a 0
  			if(i==1)
  				niceFunc(i,4);
  			if(i==2)
  				niceFunc(i,3);
  			if(i==3)
  				niceFunc(i,2);
    		if(i==4)
    			niceFunc(i,1);
    		if(i==5)
	    		niceFunc(i,1);
    		if(i==6)  
  				niceFunc(i,2);
  			if(i==7)
  				niceFunc(i,3);
  			if(i==8)	
  				niceFunc(i,4);
  			if(i==9)
  				niceFunc(i,5);
    	exit(0);
  		}
	}
	for (i=0;i<n;i++)
		wait(0); //espera a que cada uno de los hijos acabe 
	}
	else{
		wait(0);
		printf("PID proceso hijo principal: %d\n",pidCh);
	}
}
void niceFunc(int s,int m){ //Esta funcion crea los hijos hacia abajo y hacia los lados 
	int i,j,pidCh,pids[max_cant_pids];	//Siempre se tiene la misma cantidad de procesos hijos...
	printf("[hijo %d]	[PID %d] [PPID %d]\n",s+1,getpid(),getppid()); //<- obtiene la el PID actual y el del padre (primer fork)
	if(m>=2)					//<- el caso donde son menos de dos es especial
	{
	for(j=0;j<m;j++)			//...hacia abajo y en el ultimo hijo hacia los lados
    	{						//recibe un contador (estetico) y la cantidad de hijos a crear	
    		if((pidCh=fork())==0)
    		{
    			if(j!=(m-1)) //verifica si el hijo es terminal o no
					printf("[hijo de %i] [PPID %d], [PID %d]\n",s+1, getppid(),getpid());
				else
					printf("[hijoTERMINAL de %i] [PPID %d], [PID %d]\n",s+1, getppid(),getpid());					
				if(j==(m-1))			//Se verifica que sea el ultimo hijo
				{	
					for(i=0;i<m;i++)	//Este for crea los hijos hacia los lados
					{ 
						if((pids[i]=fork())==0)
						{
							printf("[HijoHoriz%d de (PPID)%d]	[PID %d] \n",i+1,getppid(),getpid());
							exit(0);
						}
						for(j=0;j<m;j++)
							wait(0);
					}
				}
    		}
    		else
    			break; //para crear hijos dentro de los hijos
    	}
    	wait(0);
    }
   	else if(m==1) //caso donde solo tendra un hijo
   	{
   		if((pidCh=fork())==0)
   		{
				printf("[hijo de %i] [PPID %d], [PID %d]\n",s+1, getppid(),getpid());
        if(fork()==0)
        {
        printf("[hijoTERMINAL de %i] [PPID %d], [PID %d]\n",s+1, getppid(),getpid());
        exit(0);
        }
        wait(0);
				exit(0);
   		}
   		else
   		{
   			wait(0);
   			exit(0); 
   		}
 	}
}