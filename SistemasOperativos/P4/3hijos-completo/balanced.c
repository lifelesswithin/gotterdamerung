#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
bool is_balanced(char expr[]) 
{
 int s = strlen(expr);
 float strlenhalf = s/2;
 int cpares = 0;
 int diferencia = 0;
        if((s %2) != 0) 
        {
        return 0; //false
        }
    for(int i = 0; i < s-1; i++) //de la izquierda
        {
        if(expr[i] == 40) //40 = "(")
            {
            for(int j = s-1; j > 0; j--) //de la derecha
                {
                        if(expr[j] == 41) //41 = ")"
                            {
                            expr[i] = 0;
                            expr[j] = 0;
                            diferencia = j- i;
                            cpares = cpares+1;
                            break;
                    }
                }            
             }
        }
    for(int i = 0; i < s-1; i++) //de la izquierda
        {
        if(expr[i] == 91) //40 = "[")
            {
            for(int j = s-1; j > 0; j--) //de la derecha
                {
                        if(expr[j] == 93) //41 = "]"
                            {
                            expr[i] = 0;
                            expr[j] = 0;
                            diferencia = j- i;
                            cpares = cpares+1;
                            break;
                            }
                }            
             }
        }
    for(int i = 0; i < s-1; i++) //de la izquierda
        {
        if(expr[i] == 123) //123 = "{")
            {
            for(int j = s-1; j > 0; j--) //de la derecha
                {
                    if(expr[j] == 125) //125 = "}"
                            {
                            expr[i] = 0;
                            expr[j] = 0;
                            diferencia = j- i;
                            cpares = cpares+1;
                            break;
                    }
                }            
             }
        }
    if(strlenhalf > cpares)
        {
        return 0; //false
        }
    return 1;
}

int main(int argc, char *argv[]){
        printf("[BALANCEO DE PARENTESIS] PPID: %d PID: %d\n",getppid(),getpid());
        char cad[200];
        memset(cad, '\0', sizeof(cad)); //Poner caracter nulo
        strcpy(cad, argv[1]);
        int answer = is_balanced(argv[1]);
        if(answer)
          printf("La expresion %s esta balanceada.\n",cad);
        else
          printf("La expresion %s no esta balanceada.\n",cad);
    exit(0);
}
