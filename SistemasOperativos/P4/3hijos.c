#include <stdio.h>   	//Borrado de archivos de texto
#include <sys/wait.h>	//Compilar programa
#include <unistd.h>		//gcc -Wall -lm 1.c -o 
#include <stdlib.h>		//InsEnMat (matrizFuente, IndiceA,IndiceB)
#include "testOK.h"		//ImpMat(matrizOrigen,IndiceA,IndiceB,Etiqueta,NoMostrarDecimales(1),NoImprimirAArchivo(1))
int main(){		
		int i;
			if(fork()==0)
			{
				printf("----[HIJO QUE CAMBIA PERMISOS] PPID: %d PID: %d\n",getppid(),getpid());
				char *argv[] = {"./cambiarpermisos", "test", "TRANSPUESTA_A", "0755", 0};
  				execvp("./cambiarpermisos", argv);
				exit(0);
			}
			wait(0);
			if(fork()==0)
			{
				int error;
				printf("----[HIJO QUE VERIFICA BALANCEO] PPID: %d PID: %d\n",getppid(),getpid());
				char *argv[] = {"./balancepar", "{}{}(()())", 0};
  				execvp("./balancepar", argv);  
				exit(0);
			}
			wait(0);
			if(fork()==0)
			{
				printf("PPID: %d PID: %d\n",getppid(),getpid());
				exit(0);
			}
			wait(0);
}