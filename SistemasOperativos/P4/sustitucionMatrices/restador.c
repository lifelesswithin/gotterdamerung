#include <stdio.h>   	//Borrado de archivos de texto
#include <sys/mman.h>	//rm INV* RES* MULT* SU* TRANS*
#include <sys/wait.h>	//Compilar programa
#include <unistd.h>		//gcc -Wall -lm 1.c -o 1
#include <stdlib.h>		//InsEnMat (matrizFuente, IndiceA,IndiceB)
#include "testOK.h"		//ImpMat(matrizOrigen,IndiceA,IndiceB,Etiqueta,NoMostrarDecimales(1),NoImprimirAArchivo(1))
#define SIZ_MAX 10		//<= LargoXAncho de la matriz, cambiar a 3 para el demo
int main(){				//ESTE SIZE_MAX TIENE QUE SER IGUAL AL DE LA BIBLIOTECA
			//PROCESO SUMA.
			printf("\n----[PROCESO QUE REALIZA LA RESTA EN EJECUCION] PPID: %d PID: %d\n",getppid(),getpid());
			float a[SIZ_MAX][SIZ_MAX],b[SIZ_MAX][SIZ_MAX],c[SIZ_MAX][SIZ_MAX];
			CargarEnArreglo("MATRIZ A ORIGINAL",a,SIZ_MAX);
			CargarEnArreglo("MATRIZ B ORIGINAL",b,SIZ_MAX);
			SumaOResta (a, b, c, SIZ_MAX, SIZ_MAX, 0);
			ImpMat(c,SIZ_MAX,SIZ_MAX,"RESTA-EXECV",1,0,1);
		}