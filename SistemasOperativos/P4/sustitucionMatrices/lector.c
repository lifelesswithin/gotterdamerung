#include <stdio.h>   	//Borrado de archivos de texto
#include <sys/mman.h>	//rm INV* RES* MULT* SU* TRANS*
#include <sys/wait.h>	//Compilar programa
#include <unistd.h>		//gcc -Wall -lm 1.c -o 1
#include <stdlib.h>		//InsEnMat (matrizFuente, IndiceA,IndiceB)
#include "testOK.h"		//ImpMat(matrizOrigen,IndiceA,IndiceB,Etiqueta,NoMostrarDecimales(1),NoImprimirAArchivo(1),NoMostrar(1))
#define SIZ_MAX 10		//<= LargoXAncho de la matriz, cambiar a 3 para el demo
int main(){				//ESTE SIZE_MAX TIENE QUE SER IGUAL AL DE LA BIBLIOTECA
			//PROCESO SUMA.
			printf("\n----[PROCESO QUE REALIZA LA SUMA EN EJECUCION] PPID: %d PID: %d\n",getppid(),getpid());
			LeerArchivos("MATRIZ A ORIGINAL");
			LeerArchivos("MATRIZ B ORIGINAL");
			LeerArchivos("RESTA-EXECV");
			LeerArchivos("SUMA-EXECV");
			LeerArchivos("MULTIPLICACION-EXECV");
			LeerArchivos("TRANSPUESTA-A-EXECV");
			LeerArchivos("TRANSPUESTA-B-EXECV");
			LeerArchivos("INVERSA-A-EXECV");
			LeerArchivos("INVERSA-B-EXECV");
		}