#include <stdio.h>   	//Borrado de archivos de texto
#include <sys/mman.h>	//rm INV* RES* MULT* SU* TRANS*
#include <sys/wait.h>	//Compilar programa
#include <unistd.h>		//gcc -Wall -lm 1.c -o 1
#include <stdlib.h>		//InsEnMat (matrizFuente, IndiceA,IndiceB)
#include "testOK.h"		//ImpMat(matrizOrigen,IndiceA,IndiceB,Etiqueta,NoMostrarDecimales(1),NoImprimirAArchivo(1))
#define SIZ_MAX 10		//<= LargoXAncho de la matriz, cambiar a 3 para el demo
int main(){				//ESTE SIZE_MAX TIENE QUE SER IGUAL AL DE LA BIBLIOTECA
			float a[SIZ_MAX][SIZ_MAX],b[SIZ_MAX][SIZ_MAX];
			printf("[PADRE | PID: %d]\n",getpid());
			printf("Introduzca el arreglo B:\n");
			InsEnMat(a,SIZ_MAX,SIZ_MAX);
			printf("Introduzca el arreglo B:\n");
			InsEnMat(b,SIZ_MAX,SIZ_MAX);
			printf("Esta es la matriz A:\n");
			ImpMat(a,SIZ_MAX,SIZ_MAX,"MATRIZ A ORIGINAL",1,0,0);
			printf("Esta es la matriz B:\n");
			ImpMat(b,SIZ_MAX,SIZ_MAX,"MATRIZ B ORIGINAL",1,0,0);
			if(fork()==0)
			{		
				printf("sumador | [hijo a morir] PID: %d PPID: %d",getpid(),getppid());
				char *argv[] = {"./sumador", 0};
  				execv("./sumador", argv);  
  				exit(0);
			}
				wait(0);	
			if(fork()==0)
			{		
				printf("restador | [hijo a morir] PID: %d PPID: %d",getpid(),getppid());
				char *argv[] = {"./restador", 0};
  				execv("./restador", argv);  
  				exit(0);
			}
				wait(0);	
			if(fork()==0)
			{		
				printf("multiplicador | [hijo a morir] PID: %d PPID: %d",getpid(),getppid());
				char *argv[] = {"./multiplicar", 0};
  				execv("./multiplicar", argv);  
  				exit(0);
			}
				wait(0);		
			if(fork()==0)
			{		
				printf("transpuestas | [hijo a morir] PID: %d PPID: %d",getpid(),getppid());
				char *argv[] = {"./transpuesta", 0};
  				execv("./transpuesta", argv);  
  				exit(0);
			}
				wait(0);		
			if(fork()==0)
			{		
				printf("inversas | [hijo a morir] PID: %d PPID: %d",getpid(),getppid());
				char *argv[] = {"./inversor", 0};
  				execv("./inversor", argv);  
  				exit(0);
			}
				wait(0);			
			if(fork()==0)
			{		
				printf("lector | [hijo a morir] PID: %d PPID: %d",getpid(),getppid());
				char *argv[] = {"./lector", 0};
  				execv("./lector", argv);  
  				exit(0);
			}
				wait(0);
		}