module practica1 ( 
	a,
	b,
	c,
	d,
	sel,
	ref,
	display
	) ;

input [0:1] a;
input [0:1] b;
input [0:1] c;
input [0:1] d;
input [0:1] sel;
input [0:1] ref;
inout [0:6] display;
