library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Practica1 is
    Port ( a : in  STD_LOGIC_VECTOR (0 to 1);
           b : in  STD_LOGIC_VECTOR (0 to 1);
           c : in  STD_LOGIC_VECTOR (0 to 1);
           d : in  STD_LOGIC_VECTOR (0 to 1);
           sel : in  STD_LOGIC_VECTOR (0 to 1);
           ref : in  STD_LOGIC_VECTOR (0 to 1);
           display : out  STD_LOGIC_VECTOR (0 to 6));
ATTRIBUTE PIN_NUMBERS OF Practica1: ENTITY IS
"a(0):1 a(1):2 b(0):3 b(1):4 c(0):5 c(1):6 d(0):7 d(1):8 sel(0):9 sel(1):10 ref(0):11 ref(1):13 display(0):17 display(1):18 display(2):19  display(3):20 display(4):21 display(5):22 display(6):23";
end Practica1;
architecture A_Practica1 of Practica1 is
SIGNAL ZZ: STD_LOGIC_VECTOR (0 TO 1); 
SIGNAL SAL: STD_LOGIC_VECTOR (2 DOWNTO 0); 
CONSTANT IGUAL3: STD_LOGIC_VECTOR (0 TO 2):="100";
CONSTANT MAYOR3: STD_LOGIC_VECTOR (0 TO 2):="010";
CONSTANT MENOR3: STD_LOGIC_VECTOR (0 TO 2):="001";
CONSTANT IGUAL: STD_LOGIC_VECTOR (0 TO 6):="0110111"; 
CONSTANT MAYOR: STD_LOGIC_VECTOR (0 TO 6):="0000111"; 
CONSTANT MENOR: STD_LOGIC_VECTOR (0 TO 6):="0110001"; 
begin
multiplexor: block --BLOQUE DEL MULTIPLEXOR
begin			 
	with sel select
			ZZ <= a when "00",
				  b when "01",
				  c when "10",
				  d when others; --when 11 
end block multiplexor; --FIN MULTIPLEXOR
comparador: block --BLOQUE DEL COMPARADOR
begin
	process(SAL,ZZ)
	begin
	if ZZ="00" and ref="00" then --1ref es 00
		SAL <= IGUAL3;
	elsif ZZ="01" and ref="00" then --2ref es 00
		SAL <= MAYOR3;
	elsif ZZ="10" and ref="00" then --3ref es 00
		SAL <= MAYOR3;
	elsif ZZ="11" and ref="00" then --4ref es 00
		SAL <= MAYOR3;
	elsif ZZ="00" and ref="01" then --1ref es 01
		SAL <= MENOR3;
	elsif ZZ="01" and ref="01" then --2ref es 01
		SAL <= IGUAL3;
	elsif ZZ="10" and ref="01" then --3ref es 01
		SAL <= MAYOR3;
	elsif ZZ="11" and ref="01" then --4ref es 01
		SAL <= MAYOR3;
	elsif ZZ="00" and ref="10" then --1ref es 10
		SAL <= MENOR3;
	elsif ZZ="01" and ref="10" then --2ref es 10
		SAL <= MENOR3;
	elsif ZZ="10" and ref="10" then --3ref es 10
		SAL <= IGUAL3;
	elsif ZZ="11" and ref="10" then --4ref es 10
		SAL <= MAYOR3;
	elsif ZZ="00" and ref="11" then --1ref es 11
		SAL <= MENOR3;
	elsif ZZ="01" and ref="11" then --2ref es 11
		SAL <= MENOR3;
	elsif ZZ="10" and ref="11" then --3ref es 11
		SAL <= MENOR3;
	elsif ZZ="11" and ref="11" then --4ref es 11
		SAL <= IGUAL3;
	end if;
	end process; --FIN COMPARADOR
end block comparador;	
decodificador: block --BLOCK DEL DECODIFICADOR-DISPLAY
begin
process(SAL)
	begin
	 	if SAL=IGUAL3 then
			display <= IGUAL;
		elsif SAL=MENOR3 then
			display <= MENOR;
		elsif SAL=MAYOR3 then
			display <= MAYOR;
		end if;
end process;
end block decodificador; --FIN DECODIFICADOR-DISPLAY
end architecture;

