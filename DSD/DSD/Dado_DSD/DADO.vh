module dado ( 
	clk,
	clr,
	c,
	d
	) ;

input  clk;
input  clr;
input  c;
inout [6:0] d;
