module variables ( 
	clk,
	clr,
	pre,
	sr,
	q,
	qn
	) ;

input  clk;
input  clr;
input  pre;
input [1:0] sr;
inout  q;
inout  qn;
