
--  CYPRESS NOVA XVL Structural Architecture

--  JED2VHD Reverse Assembler - 6.3 IR 35


--    VHDL File: SELECT_PRIORIDAD.vhd

--    Date: Fri Nov 02 15:26:16 2018

--  Disassembly from Jedec file for: c22v10

--  Device Ordercode is: PALC22V10D-25PC
library ieee;
use ieee.std_logic_1164.all;

library primitive;
use primitive.primitive.all;


-- Beginning Test Bench Header

ENTITY variables IS
    PORT (
	              numero :    in std_logic_vector (9 downto 0) ;
	                   e : inout std_logic_vector (3 downto 0)
    );
END variables;

-- End of Test Bench Header

ARCHITECTURE DSMB of variables is

	signal jed_node1	: std_logic:='0' ;
	signal jed_node2	: std_logic:='0' ;
	signal jed_node3	: std_logic:='0' ; -- numero(1)
	signal jed_node4	: std_logic:='0' ; -- numero(2)
	signal jed_node5	: std_logic:='0' ; -- numero(3)
	signal jed_node6	: std_logic:='0' ; -- numero(4)
	signal jed_node7	: std_logic:='0' ; -- numero(5)
	signal jed_node8	: std_logic:='0' ; -- numero(6)
	signal jed_node9	: std_logic:='0' ; -- numero(7)
	signal jed_node10	: std_logic:='0' ; -- numero(8)
	signal jed_node11	: std_logic:='0' ; -- numero(9)
	signal jed_node12	: std_logic:='0' ;
	signal jed_node13	: std_logic:='0' ;
	signal jed_node18	: std_logic:='0' ;
	signal jed_node19	: std_logic:='0' ;
	signal jed_node20	: std_logic:='0' ;
	signal jed_node21	: std_logic:='0' ;
	signal jed_node22	: std_logic:='0' ;
	signal jed_node23	: std_logic:='0' ;
	signal jed_node24	: std_logic:='0' ;

	for all: c22v10m use entity primitive.c22v10m (sim);

SIGNAL  one:std_logic:='1';
SIGNAL  zero:std_logic:='0';
SIGNAL  jed_oept_1:std_logic:='0';
--Attribute PIN_NUMBERS of jed_node1:SIGNAL is "0001";

SIGNAL  jed_oept_3:std_logic:='0';
--Attribute PIN_NUMBERS of numero(1):SIGNAL is "0003";

SIGNAL  jed_oept_4:std_logic:='0';
--Attribute PIN_NUMBERS of numero(2):SIGNAL is "0004";

SIGNAL  jed_oept_5:std_logic:='0';
--Attribute PIN_NUMBERS of numero(3):SIGNAL is "0005";

SIGNAL  jed_oept_6:std_logic:='0';
--Attribute PIN_NUMBERS of numero(4):SIGNAL is "0006";

SIGNAL  jed_oept_7:std_logic:='0';
--Attribute PIN_NUMBERS of numero(5):SIGNAL is "0007";

SIGNAL  jed_oept_8:std_logic:='0';
--Attribute PIN_NUMBERS of numero(6):SIGNAL is "0008";

SIGNAL  jed_oept_9:std_logic:='0';
--Attribute PIN_NUMBERS of numero(7):SIGNAL is "0009";

SIGNAL  jed_oept_10:std_logic:='0';
--Attribute PIN_NUMBERS of numero(8):SIGNAL is "0010";

SIGNAL  jed_oept_11:std_logic:='0';
--Attribute PIN_NUMBERS of numero(9):SIGNAL is "0011";

SIGNAL  jed_oept_14:std_logic:='0';
SIGNAL  jed_sum_14,jed_fb_14:std_logic:='0';
--Attribute PIN_NUMBERS of e(0):SIGNAL is "0014";

SIGNAL  jed_oept_15:std_logic:='0';
SIGNAL  jed_sum_15,jed_fb_15:std_logic:='0';
--Attribute PIN_NUMBERS of e(1):SIGNAL is "0015";

SIGNAL  jed_oept_16:std_logic:='0';
SIGNAL  jed_sum_16,jed_fb_16:std_logic:='0';
--Attribute PIN_NUMBERS of e(2):SIGNAL is "0016";

SIGNAL  jed_oept_17:std_logic:='0';
SIGNAL  jed_sum_17,jed_fb_17:std_logic:='0';
--Attribute PIN_NUMBERS of e(3):SIGNAL is "0017";

SIGNAL  jed_oept_25:std_logic:='0';
SIGNAL  jed_node25,jed_sum_25:std_logic:='0';
SIGNAL  jed_oept_26:std_logic:='0';
SIGNAL  jed_node26,jed_sum_26:std_logic:='0';

BEGIN
jed_node3 <= numero(1) ;
jed_node4 <= numero(2) ;
jed_node5 <= numero(3) ;
jed_node6 <= numero(4) ;
jed_node7 <= numero(5) ;
jed_node8 <= numero(6) ;
jed_node9 <= numero(7) ;
jed_node10 <= numero(8) ;
jed_node11 <= numero(9) ;
Mcell_14:c22v10m
generic map(comb,
   ninv,
   xpin,
   	25 ns, --tpd
	25 ns, --tea
	25 ns, --ter
	15 ns, --tco
	18 ns, --ts
	0 ns, --th
	14 ns, --twh
	14 ns, --twl
	13 ns, --tcf
	25 ns, --taw
	25 ns, --tar
	25 ns, --tap
	25 ns  --tspr
)
port map(
     d=>jed_sum_14,
     clk=>jed_node1,
     oe=>jed_oept_14,
     ss=>jed_sum_26,
     ar=>jed_sum_25,
     y=>e(0),
     fb=>jed_fb_14
   );

Mcell_15:c22v10m
generic map(comb,
   ninv,
   xpin,
   	25 ns, --tpd
	25 ns, --tea
	25 ns, --ter
	15 ns, --tco
	18 ns, --ts
	0 ns, --th
	14 ns, --twh
	14 ns, --twl
	13 ns, --tcf
	25 ns, --taw
	25 ns, --tar
	25 ns, --tap
	25 ns  --tspr
)
port map(
     d=>jed_sum_15,
     clk=>jed_node1,
     oe=>jed_oept_15,
     ss=>jed_sum_26,
     ar=>jed_sum_25,
     y=>e(1),
     fb=>jed_fb_15
   );

Mcell_16:c22v10m
generic map(comb,
   invt,
   xpin,
   	25 ns, --tpd
	25 ns, --tea
	25 ns, --ter
	15 ns, --tco
	18 ns, --ts
	0 ns, --th
	14 ns, --twh
	14 ns, --twl
	13 ns, --tcf
	25 ns, --taw
	25 ns, --tar
	25 ns, --tap
	25 ns  --tspr
)
port map(
     d=>jed_sum_16,
     clk=>jed_node1,
     oe=>jed_oept_16,
     ss=>jed_sum_26,
     ar=>jed_sum_25,
     y=>e(2),
     fb=>jed_fb_16
   );

Mcell_17:c22v10m
generic map(comb,
   invt,
   xpin,
   	25 ns, --tpd
	25 ns, --tea
	25 ns, --ter
	15 ns, --tco
	18 ns, --ts
	0 ns, --th
	14 ns, --twh
	14 ns, --twl
	13 ns, --tcf
	25 ns, --taw
	25 ns, --tar
	25 ns, --tap
	25 ns  --tspr
)
port map(
     d=>jed_sum_17,
     clk=>jed_node1,
     oe=>jed_oept_17,
     ss=>jed_sum_26,
     ar=>jed_sum_25,
     y=>e(3),
     fb=>jed_fb_17
   );

jed_node25<=jed_sum_25;
jed_node26<=jed_sum_26;
jed_oept_14<=(one);

jed_sum_14<= (((jed_node3) and not(jed_node4) and not(jed_node6) and not(jed_node8)
 and not(jed_node10)) or
((jed_node5) and not(jed_node6) and not(jed_node8) and not(jed_node10)
) or
((jed_node7) and not(jed_node8) and not(jed_node10)
) or
((jed_node9) and not(jed_node10)) or
((jed_node11)));

jed_oept_15<=(one);

jed_sum_15<= (((jed_node5) and not(jed_node6) and not(jed_node7) and not(jed_node10)
 and not(jed_node11)) or
((jed_node4) and not(jed_node6) and not(jed_node7) and not(jed_node10)
 and not(jed_node11)) or
((jed_node9) and not(jed_node10) and not(jed_node11)
) or
((jed_node8) and not(jed_node10) and not(jed_node11)
));

jed_oept_16<=(one);

jed_sum_16<= ((not(jed_node6) and not(jed_node7) and not(jed_node8)
 and not(jed_node9)) or
((jed_node11)) or
((jed_node10)));

jed_oept_17<=(one);

jed_sum_17<= ((not(jed_node10) and not(jed_node11)));

end DSMB;
