LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
ENTITY VARIABLES IS PORT(
    CLK, CLR: IN STD_LOGIC;
    C: IN STD_LOGIC_VECTOR (1 DOWNTO 0);
    E: IN STD_LOGIC_VECTOR (3 DOWNTO 0);
    DISPLAY: INOUT STD_LOGIC_VECTOR (6 DOWNTO 0) 
);
END VARIABLES;
ARCHITECTURE CONT_DECADA OF VARIABLES IS 
    CONSTANT CERO: STD_LOGIC_VECTOR (6 DOWNTO 0):="1000000";
    CONSTANT UNO: STD_LOGIC_VECTOR (6 DOWNTO 0):="1111001";
    CONSTANT DOS: STD_LOGIC_VECTOR (6 DOWNTO 0):="0100100";
    CONSTANT TRES: STD_LOGIC_VECTOR (6 DOWNTO 0):="0110000";
    CONSTANT CUATRO: STD_LOGIC_VECTOR (6 DOWNTO 0):="0011001";
    CONSTANT CINCO: STD_LOGIC_VECTOR (6 DOWNTO 0):="0010010";
    CONSTANT SEIS: STD_LOGIC_VECTOR (6 DOWNTO 0):="0000010";
    CONSTANT SIETE: STD_LOGIC_VECTOR (6 DOWNTO 0):="1111000";
    CONSTANT OCHO: STD_LOGIC_VECTOR (6 DOWNTO 0):="0000000";
    CONSTANT NUEVE: STD_LOGIC_VECTOR (6 DOWNTO 0):="0011000";
BEGIN
    PROCESS (CLK,CLR,C,DISPLAY,E) BEGIN 
        IF (CLR='0') THEN
            DISPLAY<=CERO;
        ELSIF (CLK'EVENT AND CLK='1') THEN
            CASE C IS
                WHEN "00"=>
                    CASE DISPLAY IS 
                        WHEN CERO=>DISPLAY<=UNO;
                        WHEN UNO=>DISPLAY<=DOS;
                        WHEN DOS=>DISPLAY<=TRES;
                        WHEN TRES=>DISPLAY<=CUATRO;
                        WHEN CUATRO=>DISPLAY<=CINCO;
                        WHEN CINCO=>DISPLAY<=SEIS;
                        WHEN SEIS=>DISPLAY<=SIETE;
                        WHEN SIETE=>DISPLAY<=OCHO;
                        WHEN OCHO=>DISPLAY<=NUEVE;
                        WHEN OTHERS=>DISPLAY<=CERO;
                    END CASE;
                WHEN "01"=>
                    CASE DISPLAY IS 
                        WHEN CERO=>DISPLAY<=NUEVE;
                        WHEN NUEVE=>DISPLAY<=OCHO;
                        WHEN OCHO=>DISPLAY<=SIETE;
                        WHEN SIETE=>DISPLAY<=SEIS;
                        WHEN SEIS=>DISPLAY<=CINCO;
                        WHEN CINCO=>DISPLAY<=CUATRO;
                        WHEN CUATRO=>DISPLAY<=TRES;
                        WHEN TRES=>DISPLAY<=DOS;
                        WHEN DOS=>DISPLAY<=UNO;
                        WHEN OTHERS=>DISPLAY<=CERO;
                    END CASE;
                WHEN "10"=>
                    DISPLAY<=DISPLAY;
                WHEN OTHERS=>
                    CASE E IS
                        WHEN "0000"=>DISPLAY<=CERO;
                        WHEN "0001"=>DISPLAY<=UNO;
                        WHEN "0010"=>DISPLAY<=DOS;
                        WHEN "0011"=>DISPLAY<=TRES;
                        WHEN "0100"=>DISPLAY<=CUATRO;
                        WHEN "0101"=>DISPLAY<=CINCO;
                        WHEN "0110"=>DISPLAY<=SEIS;
                        WHEN "0111"=>DISPLAY<=SIETE;
                        WHEN "1000"=>DISPLAY<=OCHO;
                        WHEN "1001"=>DISPLAY<=NUEVE;
                        WHEN OTHERS=>DISPLAY<=CERO;
                    END CASE;
            END CASE;
        END IF;
    END PROCESS;
END CONT_DECADA;