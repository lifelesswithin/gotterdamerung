#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#gcc -Wall -lcrypto 666.c -o encryptdecrypt
import wx
from subprocess import DEVNULL, STDOUT, check_call
def basicencryptdecrypt(key,ifile,ofile,mode):
    print("[INFO] Summoning backend with: [./encryptdecrypt",ifile,ofile,key,mode,"]")
    try:
        call = check_call(['./encryptdecrypt', ifile, ofile, key, str(mode)])
        print("[INFO] Backend ended with exit code:",call)
        return 0
    except Exception as e: 
        print("[ERROR] Wrong key!")
        return e.returncode

class MyFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.filestuff = ""
        self.key = 0
        self.ifile = ""
        self.filestatus = 0
        self.keystatus = 0
        self.mode = 0
        self.SetSize((400, 80))
        self.panel_1 = wx.Panel(self, wx.ID_ANY)
        self.combo_box_1 = wx.ComboBox(self.panel_1, wx.ID_ANY, choices=["Action...", "Encrypt", "Decrypt"], style=wx.CB_DROPDOWN | wx.CB_READONLY | wx.TE_PROCESS_ENTER)
        self.text_ctrl_1 = wx.TextCtrl(self.panel_1, wx.ID_ANY, "", style=wx.TE_NO_VSCROLL | wx.TE_PROCESS_ENTER)
        self.button_1 = wx.Button(self.panel_1, wx.ID_ANY, "OK")
        self.__set_properties()
        self.__do_layout()
        self.Bind(wx.EVT_COMBOBOX, self.oncomboboxchange, self.combo_box_1)
        self.Bind(wx.EVT_TEXT, self.ontextinputhandler, self.text_ctrl_1)
        self.Bind(wx.EVT_BUTTON, self.okbutton, self.button_1)

    def __set_properties(self):
        self.SetTitle("Juan Méndez' encryption tool 2.0")
        self.combo_box_1.SetSelection(0)

    def __do_layout(self):
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_4 = wx.BoxSizer(wx.VERTICAL)
        sizer_3 = wx.BoxSizer(wx.VERTICAL)
        label_1 = wx.StaticText(self.panel_1, wx.ID_ANY, "Choose an action")
        sizer_3.Add(label_1, 0, wx.ALIGN_CENTER, 0)
        sizer_3.Add(self.combo_box_1, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALL | wx.EXPAND, 0)
        sizer_2.Add(sizer_3, 1, wx.EXPAND, 0)
        label_2 = wx.StaticText(self.panel_1, wx.ID_ANY, "Key:")
        sizer_4.Add(label_2, 0, wx.ALIGN_CENTER, 0)
        sizer_4.Add(self.text_ctrl_1, 0, wx.EXPAND, 0)
        sizer_2.Add(sizer_4, 1, wx.EXPAND, 0)
        sizer_2.Add(self.button_1, 0, wx.EXPAND, 0)
        self.panel_1.SetSizer(sizer_2)
        sizer_1.Add(self.panel_1, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade

    def oncomboboxchange(self, event):  # This function will trigger whenever the thing in te combobox changes. 
        if(not self.combo_box_1.GetStringSelection().startswith("Action")): 
            with wx.FileDialog(self, "Open file", wildcard="TXT files (*.txt)|*.txt",style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:
                if fileDialog.ShowModal() == wx.ID_CANCEL:  
                    self.combo_box_1.SetSelection(0)
                    return
                else:   
                    try:
                            self.ifile = fileDialog.GetPath()
                            self.filestatus = 1
                            self.mode = self.combo_box_1.GetStringSelection()   #This can be either 'Decrypt' or 'Encrypt'
                            return
                    except Exception as e: wx.MessageBox("There's been an internal error: \n"+str(e), "Hell no!",  wx.OK | wx.ICON_ERROR)  
        self.mode = 0

    def ontextinputhandler(self, event):  # Validate key value 
        value = self.text_ctrl_1.GetValue()
        if(len(value)>=14):
            self.key = str(value)
            self.keystatus = 1
            print("[INFO] KeyStatus updated (1)")
            return 
        print("[INFO] KeyStatus updated (0)")
        self.keystatus = 0


    def okbutton(self, event):  # Here, we do all the things we need to do whenever the OK button is pressed
        print("[INFO] KeyStatus:",self.keystatus,"FileStatus:",self.filestatus,"Mode:",self.mode)
        if(self.mode == 0):     #Return if no action has been selected
            wx.MessageBox("You must choose an action!", 'Error', wx.OK | wx.ICON_ERROR)
            return  
        if(self.keystatus and self.filestatus):
            with wx.FileDialog(self, "Save "+self.mode+"ed file", defaultFile=self.mode+"ed.txt", wildcard="TXT files (*.txt)|*.txt",
                       style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as fileDialog:
                if fileDialog.ShowModal() == wx.ID_CANCEL:  return
                try:
                    if(self.mode=="Encrypt"):
                        if(basicencryptdecrypt(self.key,self.ifile,fileDialog.GetPath(),0)==0):
                            wx.MessageBox("Backend informs that encryption has been successful", self.mode+" OK",  wx.OK | wx.ICON_INFORMATION)
                            self.combo_box_1.SetSelection(0)
                            return
                        wx.MessageBox("There's been a problem with the C backend.", self.mode+" OK",  wx.OK | wx.ICON_INFORMATION)                        
                        return
                    else:  
                        val = basicencryptdecrypt(self.key,self.ifile,fileDialog.GetPath(),1) 
                        if(val==0):
                            wx.MessageBox("Backend informs that decryption has been successful", self.mode+" OK",  wx.OK | wx.ICON_INFORMATION)
                            self.combo_box_1.SetSelection(0)
                            return
                        elif(val==2):
                            wx.MessageBox("C backend reports: Weak or wrong password", "ERROR",  wx.OK | wx.ICON_INFORMATION)                        
                            return
                        else:
                            wx.MessageBox("There's been an unknown problem with the C backend.", "ERROR",  wx.OK | wx.ICON_INFORMATION)                        
                            return
                except Exception as e: 
                    wx.MessageBox("There's been an internal error: \n"+str(e), "Zoinks!",  wx.OK | wx.ICON_ERROR)                        
                    self.combo_box_1.SetSelection(0)
            self.filestatus = 0
        else:   
            wx.MessageBox("You must choose a file/Set a key!", 'Error', wx.OK | wx.ICON_ERROR)

class MyApp(wx.App):
    def OnInit(self):
        self.frame = MyFrame(None, wx.ID_ANY, "")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
