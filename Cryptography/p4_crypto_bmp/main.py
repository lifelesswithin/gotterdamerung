#!/usr/bin/env python
# -*- coding: UTF-8 -*-
	########################################################################################################################
	#
	# ESCUELA SUPERIOR DE CÓMPUTO
	# Juan Manuel Dávila Méndez
	# Python controller for the C++ backend.
	# Made with <3 and FourLoko
	#
	# ######################################################################################################################

import wx, os, sys
from wx.lib.embeddedimage import PyEmbeddedImage
from subprocess import check_call
from PIL import Image
class PyImage():            #This class allows us to modificate BMP files.
    def __init__(self,path):    self.image = Image.open(path)   #Open Image

    def Save(self,path):    self.new.save(path, 'bmp')  #Save image as BMP

    def Modify(self,r,g,b,mode=None):
        width, height = self.image.size
        self.new = Image.new('RGB', (width,height)) # Create a new image
        pixels = self.new.load()                    # Load new image descriptor
        for i in range(width):                         
            for j in range(height):
                R,G,B = self.image.getpixel((i,j))  #Get RGB values from the source image
                pixels[i, j] = ((R+r)%256,(G+g)%256,(B+b)%256) if mode else ((R-r)%256,(G-g)%256,(B-b)%256)


def ModifyBMP(ifile,R,G,B,mode,ofile):  # This function calls the C++ backend executable with some parameters.
    BACKEND_EXECUTABLE = "./backend"
    print("[INFO] Summoning C++ backend with: [",BACKEND_EXECUTABLE,ifile,R,G,B,mode,ofile,"]")
    try:
        call = check_call([BACKEND_EXECUTABLE, ifile, str(R), str(G), str(B), str(mode), ofile])
        print("[INFO] Backend ended with exit code:",call)
        return 0
    except Exception as e: print("[ERROR] Something wrong happened!",e.returncode)

class MyFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: MyFrame.__init__
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        icon = PyEmbeddedImage('iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABghJREFUeJzt3U+rJFcdx+HvjYsgd+K4UONCkCxD3kAYcGFwqW/AF6Cg2bjIUmkzWWSRd5OAu+ig+AeEcRTdqG9As0jIGFwExsW9xzSdW32qq0796erngYLbcPlxTlMfalPVlQAAAAAAAAAAAAAAAAAAAAAAAAAAAFyEqyT3Gs164XYebMJVkneSPEnylZGzXkzytyRvRSRsQInj2e0xJpISR5klEs7aYRxjIjmMQyScta44hkTSFYdIOEu1OE6JpBaHSDgrfePoE0nfOETCWTg1jmORnBqHSFi1oXHcFcnQOETCKo2NYz+SlzMuDpGwKq3ieJbkgyT/bDRLJCxuzXGIhEWdQxwiYRHnFIdImNU5xiESZnHOcYiESW0hDpEwiS3FIRKa2mIcIqGJLcchEka5hDhEwiAt4/hXkp82miUSFtc6jldu577RaKZIWMxUcRRrj+RRkueHfnls29RxFGuN5FHa/W4XGzNXHMXaIhEHneaOo1hLJOKg01JxFEtHIg46LR1HsVQk4qDTWuIo5o5EHHRaWxzFXJGIg05rjaOYOhJx0Kn1vVXfn2idU0XyOMk3J1ozZ651HH9P8jTJtyZab+tIniT5JG3eT8IGvZTk47SLo3w+h0hKHPufRcLnPMi4SA7jOIdIDuMQCUcNjaQrjjVH0hWHSDjq1EhqcawxklocIuGovpF8kOQfPf5vTZH0jUMkHFWL5NQ41hDJqXGIhKO6Ihkax5KRDI1DJBx1GMnYOJaIZGwc5fhTPFHIHUokreKYM5JWcTxL8vpEa2UDvpu2ccwRycOIgxm8mOSvaR/HlJF8O+JgBlPHMUUk4mAWc8XRMhJxMIu542gRiTiYxdj3kC8RiTiYxdJxDInktYiDGawljlMiEQezWFscfSJpGcePB3xnXIivZZ1xHIvElWPDnlt6AQc+SfLvpRdxxHWSX+SzSF5L8l6SLzaY/Zsk34jXGlBxLzc/e7P01aJ2Jflh2l05fr3399sRCRVrj+RxpolDJPS21kimjkMk9La2SOaKQyT0tpZI5o5DJPR2nWUjWSoOkdDbUpEsHYdI6G3uSNYSh0joba5I1haHSOht6kjWGodI6O06ya+y7jh+kptbSKaIWCRUtY6kZRzlrtwXIhIW1CqS93PzNOBHDWb96GCNImFRYyN5/3ZGkryacZEcxlGIhEUNjWQ/jmJoJF1xFCJhUddJfplxcRSnRlKLoxAJi+obybE4ir6R9I2jEAmLqkXSJ46iFsmpcRQiYVFdkZwSR9EVydA4CpGwqMNIhsRRHEYyNo5CJCyqRDImjuLVJB+mXRyFSFjUdcbHUXy10ZxDIoEKkUCFSKBCJFAhEqgQCVSIBCpEAhUigQqRQIVIoEIkUCESqBAJVIgEKkQCFSKBCpFAhUigQiRQIRKoEAlUfCnJHyMS+JyrJO8keZrkSUQC/1fiKCeySODWYRwigVtdcYiEi1eLQyRcrL5xiISLc2ocIuFiDI1DJGze2DhEwma1ikMkbNLDtD+RnyZ5PMFckTC7B0k+TtuT+FGSr8cNjmxEy0geJbl3O9ddwGxGi0j24yhEwmaMieSuOAqRsBlDIjkWRyESNuOUSPrEUYiEzegTySlxFCJhM45FMiSOQiRsxl2RjImjEAmbsR9JizgKkbAZD5K8m3ZxFBcRyXNLL4DJ/TbJ93Jzn1VLX0jyaZK/NJ6bJP/NTShwlr6c5A+5OYn/k+TPaXf12M23DWhvP45ytIpkN982oL274mgVyW6+bUB7x+IYG8luvm1Ae/dTj2NoJLv5tgHtnRLHqZHs5tsGtDckjr6R7ObbBrQ3Jo5aJLv5tgHttYijK5LdfNuA9lrGcRjJbr5tQHtTxFGOn824D2jufpLfZ5o4dvNtA9oTB3QQB3QQB3SYMo6fz7gPaO5+kt9lujhW8TQgDCEO6CAO6CAO6CAOOOIHmSaONyMONuAqyVsRB3RqGYk42KQWkYiDTRsTiTi4CEMieRhxcEFOiUQcXKQ+kYiDi3YsEnFA7o5EHLBnPxJxwB2uknwn4gAAAAAAAAAAAAAAAAAAAG78D3RBZn+BbaFgAAAAAElFTkSuQmCC').GetIcon()
        self.SetIcon(icon)
        self.SetSize((629, 86))
        self.combo_box_1 = wx.ComboBox(self, wx.ID_ANY, choices=["C++ Backend", "PIL Backend"], style=wx.CB_DROPDOWN | wx.CB_READONLY)
        self.button_1 = wx.Button(self, wx.ID_ANY, "Open BMP File")
        self.text_ctrl_1 = wx.TextCtrl(self, wx.ID_ANY, "")
        self.text_ctrl_2 = wx.TextCtrl(self, wx.ID_ANY, "")
        self.text_ctrl_3 = wx.TextCtrl(self, wx.ID_ANY, "")
        self.combo_box_2 = wx.ComboBox(self, wx.ID_ANY, choices=["Shift ++", "Shift --"], style=wx.CB_DROPDOWN | wx.CB_READONLY)
        self.button_2 = wx.Button(self, wx.ID_ANY, "OK")

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_COMBOBOX, self.OnBackendChange, self.combo_box_1)
        self.Bind(wx.EVT_BUTTON, self.OnOpenPress, self.button_1)
        self.Bind(wx.EVT_TEXT, self.OnRChange, self.text_ctrl_1)
        self.Bind(wx.EVT_TEXT, self.OnGChange, self.text_ctrl_2)
        self.Bind(wx.EVT_TEXT, self.OnBChange, self.text_ctrl_3)
        self.Bind(wx.EVT_COMBOBOX, self.OnModeChange, self.combo_box_2)
        self.Bind(wx.EVT_BUTTON, self.OnOKPress, self.button_2)
        # end wxGlade

    def __set_properties(self):
        self.ifile = ""
        self.backend = "C++"
        self.R = 0
        self.G = 0
        self.B = 0
        self.SetTitle("BMP smol tool")
        self.OK = False
        self.a = False
        self.b = False
        self.c = False
        self.combo_box_1.SetSelection(0)
        self.combo_box_2.SetSelection(0)
        self.text_ctrl_1.Disable()
        self.text_ctrl_2.Disable()
        self.text_ctrl_3.Disable()
        self.combo_box_2.Disable()
        self.button_2.Disable()

        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: MyFrame.__do_layout
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_7 = wx.BoxSizer(wx.VERTICAL)
        sizer_6 = wx.BoxSizer(wx.VERTICAL)
        sizer_5 = wx.BoxSizer(wx.VERTICAL)
        sizer_4 = wx.BoxSizer(wx.VERTICAL)
        sizer_3 = wx.BoxSizer(wx.VERTICAL)
        sizer_3.Add(self.combo_box_1, 0, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
        sizer_3.Add(self.button_1, 1, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
        sizer_2.Add(sizer_3, 1, wx.EXPAND, 0)
        label_1 = wx.StaticText(self, wx.ID_ANY, "     R:")
        sizer_4.Add(label_1, 1, wx.ALIGN_CENTER | wx.TOP, 3)
        sizer_4.Add(self.text_ctrl_1, 0, wx.ALL | wx.EXPAND, 0)
        sizer_2.Add(sizer_4, 1, wx.EXPAND, 0)
        label_2 = wx.StaticText(self, wx.ID_ANY, "     G:")
        sizer_5.Add(label_2, 1, wx.ALIGN_CENTER | wx.TOP, 3)
        sizer_5.Add(self.text_ctrl_2, 0, wx.ALL | wx.EXPAND, 0)
        sizer_2.Add(sizer_5, 1, wx.EXPAND, 0)
        label_3 = wx.StaticText(self, wx.ID_ANY, "     B:")
        sizer_6.Add(label_3, 1, wx.ALIGN_CENTER | wx.TOP, 3)
        sizer_6.Add(self.text_ctrl_3, 0, wx.ALL | wx.EXPAND, 0)
        sizer_2.Add(sizer_6, 1, wx.EXPAND, 0)
        sizer_7.Add(self.combo_box_2, 0, wx.ALL | wx.EXPAND, 0)
        sizer_7.Add(self.button_2, 1, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
        sizer_2.Add(sizer_7, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_2)
        self.Layout()
        # end wxGlade

    def OnBackendChange(self, event):  # wxGlade: MyFrame.<event_handler>
        if(self.combo_box_1.GetValue()=="C++ Backend"):
            self.backend = "C++"
            print("[INFO] Using C++ backend")
        else:
            print("[INFO] Using Python backend")
            self.backend = "PIL"

    def OnOpenPress(self, event):  # wxGlade: MyFrame.<event_handler>
        if(not self.combo_box_1.GetStringSelection().startswith("Action")): 
            with wx.FileDialog(self, "Open file", wildcard="BMP files (*.bmp)|*.bmp",style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:
                if fileDialog.ShowModal() == wx.ID_CANCEL:  
                    return
                else:   
                    try:
                        self.ifile = fileDialog.GetPath()
                        self.text_ctrl_1.Enable()
                        self.text_ctrl_2.Enable()
                        self.text_ctrl_3.Enable()
                        self.combo_box_2.Enable()
                        return
                    except Exception as e: wx.MessageBox("There's been an internal error: \n"+str(e), "Hell no!",  wx.OK | wx.ICON_ERROR)  

    def OnRChange(self, event):  # wxGlade: MyFrame.<event_handler>
        value = self.text_ctrl_1.GetValue()
        if(value.isdigit()):
            value = int(value)
            if(value>=0):
                self.R = value%256
                self.a = True
                if(self.a and self.b and self.c):         self.button_2.Enable()
                print("[INFO] R is OK")
                return 
        if(len(value)==0):	
            self.a = False
            self.button_2.Disable()
        else:   
            wx.MessageBox("R must be an integer!", "Hell no!",  wx.OK | wx.ICON_ERROR)  
            self.text_ctrl_1.SetValue("0")
            self.a = False
            self.button_2.Disable()

    def OnGChange(self, event):  # wxGlade: MyFrame.<event_handler>
        value = self.text_ctrl_2.GetValue()
        if(value.isdigit()):
            value = int(value)
            if(value>=0):
                self.G = value%256
                self.b = True
                if(self.a and self.b and self.c):         self.button_2.Enable()
                print("[INFO] G is OK")
                return 
        if(len(value)==0):	
            self.b = False
            self.button_2.Disable()
        else:   
            wx.MessageBox("G must be an integer!", "Hell no!",  wx.OK | wx.ICON_ERROR)  
            self.text_ctrl_2.SetValue("0")
            self.b = False
            self.button_2.Disable()

    def OnBChange(self, event):  # wxGlade: MyFrame.<event_handler>
        value = self.text_ctrl_3.GetValue()
        if(value.isdigit()):
            value = int(value)
            if(value>=0):
                self.B = value%256
                self.c = True
                if(self.a and self.b and self.c):         self.button_2.Enable()
                print("[INFO] B is OK")
                return 
        if(len(value)==0):	
            self.c = False
            self.button_2.Disable()
        else:   
            wx.MessageBox("B must be an integer!", "Hell no!",  wx.OK | wx.ICON_ERROR)  
            self.text_ctrl_3.SetValue("0")
            self.c = False
            self.button_2.Disable()

    def OnModeChange(self, event):  event.Skip()

    def OnOKPress(self, event):  # wxGlade: MyFrame.<event_handler>
        with wx.FileDialog(self, 
        "Save modified image", 
        defaultFile= "MODIFIED.bmp", 
        wildcard="BMP files (*.bmp)|*.bmp",
        style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as outputFile:
            if outputFile.ShowModal() == wx.ID_CANCEL:  return
            else:
                try:
                    if("++" in self.combo_box_2.GetValue()):
                        if(self.backend=="C++"):
                            ModifyBMP(self.ifile,self.R,self.G,self.B,1,outputFile.GetPath())
                        else:
                            print("[INFO] Using PIL to process image")
                            PyMage = PyImage(self.ifile)  
                            PyMage.Modify(self.R,self.G,self.B,1)  
                            PyMage.Save(outputFile.GetPath())
                            del PyMage
                            print("[INFO/PIL] Success!")
                    else:
                        if(self.backend=="C++"):
                            ModifyBMP(self.ifile,self.R,self.G,self.B,0,outputFile.GetPath())
                        else:
                            print("[INFO] Using PIL to process image")
                            PyMage = PyImage(self.ifile)  
                            PyMage.Modify(self.R,self.G,self.B,0)  
                            PyMage.Save(outputFile.GetPath())
                            del PyMage
                            print("[INFO/PIL] Success!")
                    wx.MessageBox("Image saved successfully", "Hell yes!",  wx.OK | wx.ICON_INFORMATION)  
                except Exception as e:
                    wx.MessageBox("Something wrong happened!"+str(e), "Hell no!",  wx.OK | wx.ICON_ERROR)  

# end of class MyFrame

class MyApp(wx.App):
    def OnInit(self):
        self.frame = MyFrame(None, wx.ID_ANY, "")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

# end of class MyApp

if __name__ == "__main__":
    assert os.path.isfile('./backend'),"There is no C++ backend! Exiting..."
    assert "PIL" in sys.modules,"PIL is not installed! Exiting..."
    app = MyApp(0)
    app.MainLoop()
    sys.exit(0)