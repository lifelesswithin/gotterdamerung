#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# generated by wxGlade 0.9.0 on Mon Mar 18 01:02:54 2019
#

import wx
from PleasureSymbols import PleasureSymbol
# begin wxGlade: dependencies
# end wxGlade

# begin wxGlade: extracode
# end wxGlade


class theApp(wx.Frame):
	def __init__(self, *args, **kwds):
		# begin wxGlade: theApp.__init__
		kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
		wx.Frame.__init__(self, *args, **kwds)
		self.SetSize((643, 61))
		self.load_file_button = wx.Button(self, wx.ID_ANY, "Load file")
		self.save_hashed_button = wx.Button(self, wx.ID_ANY, "Save w/ Hash")
		self.integrity_check_button = wx.Button(self, wx.ID_ANY, "Integrity Check")
		self.reload_button = wx.Button(self, wx.ID_ANY, "Reload")

		self.__set_properties()
		self.__do_layout()

		self.Bind(wx.EVT_BUTTON, self.on_load_file_press, self.load_file_button)
		self.Bind(wx.EVT_BUTTON, self.on_save_press, self.save_hashed_button)
		self.Bind(wx.EVT_BUTTON, self.on_integrity_check_press, self.integrity_check_button)
		self.Bind(wx.EVT_BUTTON, self.on_get_hash_press, self.reload_button)
		# end wxGlade

	def __set_properties(self):
		# begin wxGlade: theApp.__set_properties
		self.SetTitle("Hash")
		self.save_hashed_button.Disable()
		self.integrity_check_button.Disable()
		self.reload_button.Disable()
		# end wxGlade

	def __do_layout(self):
		# begin wxGlade: theApp.__do_layout
		sizer_1 = wx.BoxSizer(wx.VERTICAL)
		sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
		sizer_2.Add(self.load_file_button, 1, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
		sizer_2.Add(self.save_hashed_button, 0, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
		sizer_2.Add(self.integrity_check_button, 0, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
		sizer_2.Add(self.reload_button, 0, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
		sizer_1.Add(sizer_2, 1, wx.EXPAND, 0)
		self.SetSizer(sizer_1)
		self.Layout()

	def __enable_buttons(self):
		self.save_hashed_button.Enable()
		self.integrity_check_button.Enable()
		self.reload_button.Enable()

		# end wxGlade

	def on_load_file_press(self, event):  # wxGlade: theApp.<event_handler>
		with wx.FileDialog(self, "Open file", wildcard="TXT files (*.txt)|*.txt",style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:
			if fileDialog.ShowModal() == wx.ID_CANCEL:		return
			else:
				try:
					self.original_name = fileDialog.GetPath()
					self.nice_symbol = PleasureSymbol(self.original_name)
					self.__enable_buttons()
				except Exception as e:  wx.MessageBox("Something wrong happened. Error:\n\n"+str(e), "Hell no!",  wx.OK | wx.ICON_ERROR)  

	def on_save_press(self, event):  # wxGlade: theApp.<event_handler>
		with wx.FileDialog(self, 
			"Save message with hash", 
			defaultFile= 'hashedMessage.txt',
			wildcard="TXT files (*.txt)|*.txt",
			style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as outputFile:
			if outputFile.ShowModal() == wx.ID_CANCEL:  return
			else:
				try:
					self.nice_symbol.save_hashed(outputFile.GetPath())
					wx.MessageBox("File saved successfully", "Hell yes!",  wx.OK | wx.ICON_INFORMATION)  
				except Exception as e:  wx.MessageBox("Something wrong happened. Error:\n\n"+str(e), "Hell no!",  wx.OK | wx.ICON_ERROR)  
			

	def on_integrity_check_press(self, event):  # wxGlade: theApp.<event_handler>
		try:
			message_str = '😊 Integrity check passed' if self.nice_symbol.integrity_check() else '😞 Integrity check failed!'
			message_str += "\n\n Message hash:	" + str(self.nice_symbol.calculate_hash(self.nice_symbol.get_original('message'))) 
			message_str += "\n\n Included hash:	" + str(self.nice_symbol.get_original('hash')) 
			wx.MessageBox(message_str, "💯🔥😂👌🏼 [LIT AF MESSAGE] 👌😂🔥💯",  wx.OK | wx.ICON_INFORMATION)  
		except Exception as e:    wx.MessageBox("Something wrong happened. Error:\n\n"+str(e), "💯🔥😂👌🏼 [LIT AF ERROR] 👌😂🔥💯",  wx.OK | wx.ICON_ERROR)  

	def on_get_hash_press(self, event):  # wxGlade: theApp.<event_handler>
		try:						self.nice_symbol = PleasureSymbol(self.original_name)
		except Exception as e:		wx.MessageBox("Something wrong happened. Error:\n\n"+str(e), "💯🔥😂👌🏼 [LIT AF ERROR] 👌😂🔥💯",  wx.OK | wx.ICON_ERROR)  
		

# end of class theApp

class MyApp(wx.App):
	def OnInit(self):
		self.frame = theApp(None, wx.ID_ANY, "")
		self.SetTopWindow(self.frame)
		self.frame.Show()
		return True

# end of class MyApp

if __name__ == "__main__":
	app = MyApp(0)
	app.MainLoop()
