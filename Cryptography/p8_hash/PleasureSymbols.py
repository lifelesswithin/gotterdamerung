#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import hashlib

class PleasureSymbol():

	def __init__(self,inputfile):
		with open(inputfile,'r') as file_descriptor:	self.file_contents = file_descriptor.read()
		self.delimiter = '\n__sha2_hash_attribute__\n'

		'''
		Esta función calcula el hash de algún string.

		Parámetros:
		data -- cadena de datos a hashear.
		'''

	def calculate_hash(self,data):
		message_hash = hashlib.sha256()
		message_hash.update(data.encode('UTF-8')) 
		return message_hash.digest().hex()

		'''
		Esta función guarda el contenido hasheado de la clase.
		Parámetros:
		output_name -- nombre del archivo a guardar.
		'''

	def save_hashed(self,output_name):
		with open(output_name,'w')	as file_descriptor:		file_descriptor.write(self.file_contents	+	self.delimiter	+	self.calculate_hash(self.file_contents))

		'''
		Esta función obtiene el contenido o el hash guardados en un archivo de texto.
		Parámetros: 
		content -- 'hash' para obtener el hash guardado. 'message' para obtener el mensaje.
		'''

	def get_original(self,content):
		try:					return self.file_contents.split(self.delimiter,1)[0]	if content=='message' else	self.file_contents.split(self.delimiter,1)[1]
		except IndexError:		raise ValueError("Your text file doesn't contain a hash")

	def integrity_check(self):	return True if self.get_original('hash') == self.calculate_hash(self.get_original('message')) else False
		

if __name__ == "__main__":
	a = PleasureSymbol('mensajesinhash.txt')
	print('Hash del mensaje archivo 1:	',a.calculate_hash(a.file_contents))
	a.save_hashed('mensajeconhash.txt')

	b = PleasureSymbol('mensajeconhash.txt')
	print('Hash incluido en el archivo 2:	',b.get_original('hash'))
	print('Hash del mensaje archivo 2:	',b.calculate_hash(b.get_original('message')))
	print(b.integrity_check())

