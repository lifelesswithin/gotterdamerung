#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import wx
from wx.lib.embeddedimage import PyEmbeddedImage
RING_KEY = 255

def enc_dec(char,key):    return int((char+key)%RING_KEY)


def basicencryptdecrypt(key,file,mode=None):		#chr(CHAR_NUM)
    words=[]										#ord(ASCII_SYMBOL)						
    errflag = 0
    for line in file:           #Read file, line by line
        for char in line:       #Read line character by character
            try:                
                #if(mode): words.append(chr(ord(char)+key))    #Encrypt mode
                if(mode): words.append(chr(enc_dec(ord(char),key)))    #Encrypt mode
                #else: words.append(chr(ord(char)-key))        #Decrypt mode
                else: 
                    if((ord(char)-key)<0):    
                        newchar = chr((ord(char)-key)%RING_KEY)
                        words.append(newchar)        #Decrypt mode
                        print("[I] neg key: ",ord(char)-key,">>",newchar)
                    else:   
                        newchar  = chr(ord(char)-key)
                        print("[I] key:",ord(char)-key,">>",newchar)
                        words.append(newchar)        #Decrypt mode
            except Exception as e:
                errflag = 1
                words.append("_")       #This can happen with a negative ascii value
                print("[E] symbol:[",char,"] val:[",ord(char)-key,"]",e)#is used with chr()
    return ''.join(words),errflag       

class MyFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        icon = PyEmbeddedImage('iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABghJREFUeJzt3U+rJFcdx+HvjYsgd+K4UONCkCxD3kAYcGFwqW/AF6Cg2bjIUmkzWWSRd5OAu+ig+AeEcRTdqG9As0jIGFwExsW9xzSdW32qq0796erngYLbcPlxTlMfalPVlQAAAAAAAAAAAAAAAAAAAAAAAAAAAFyEqyT3Gs164XYebMJVkneSPEnylZGzXkzytyRvRSRsQInj2e0xJpISR5klEs7aYRxjIjmMQyScta44hkTSFYdIOEu1OE6JpBaHSDgrfePoE0nfOETCWTg1jmORnBqHSFi1oXHcFcnQOETCKo2NYz+SlzMuDpGwKq3ieJbkgyT/bDRLJCxuzXGIhEWdQxwiYRHnFIdImNU5xiESZnHOcYiESW0hDpEwiS3FIRKa2mIcIqGJLcchEka5hDhEwiAt4/hXkp82miUSFtc6jldu577RaKZIWMxUcRRrj+RRkueHfnls29RxFGuN5FHa/W4XGzNXHMXaIhEHneaOo1hLJOKg01JxFEtHIg46LR1HsVQk4qDTWuIo5o5EHHRaWxzFXJGIg05rjaOYOhJx0Kn1vVXfn2idU0XyOMk3J1ozZ651HH9P8jTJtyZab+tIniT5JG3eT8IGvZTk47SLo3w+h0hKHPufRcLnPMi4SA7jOIdIDuMQCUcNjaQrjjVH0hWHSDjq1EhqcawxklocIuGovpF8kOQfPf5vTZH0jUMkHFWL5NQ41hDJqXGIhKO6Ihkax5KRDI1DJBx1GMnYOJaIZGwc5fhTPFHIHUokreKYM5JWcTxL8vpEa2UDvpu2ccwRycOIgxm8mOSvaR/HlJF8O+JgBlPHMUUk4mAWc8XRMhJxMIu542gRiTiYxdj3kC8RiTiYxdJxDInktYiDGawljlMiEQezWFscfSJpGcePB3xnXIivZZ1xHIvElWPDnlt6AQc+SfLvpRdxxHWSX+SzSF5L8l6SLzaY/Zsk34jXGlBxLzc/e7P01aJ2Jflh2l05fr3399sRCRVrj+RxpolDJPS21kimjkMk9La2SOaKQyT0tpZI5o5DJPR2nWUjWSoOkdDbUpEsHYdI6G3uSNYSh0joba5I1haHSOht6kjWGodI6O06ya+y7jh+kptbSKaIWCRUtY6kZRzlrtwXIhIW1CqS93PzNOBHDWb96GCNImFRYyN5/3ZGkryacZEcxlGIhEUNjWQ/jmJoJF1xFCJhUddJfplxcRSnRlKLoxAJi+obybE4ir6R9I2jEAmLqkXSJ46iFsmpcRQiYVFdkZwSR9EVydA4CpGwqMNIhsRRHEYyNo5CJCyqRDImjuLVJB+mXRyFSFjUdcbHUXy10ZxDIoEKkUCFSKBCJFAhEqgQCVSIBCpEAhUigQqRQIVIoEIkUCESqBAJVIgEKkQCFSKBCpFAhUigQiRQIRKoEAlUfCnJHyMS+JyrJO8keZrkSUQC/1fiKCeySODWYRwigVtdcYiEi1eLQyRcrL5xiISLc2ocIuFiDI1DJGze2DhEwma1ikMkbNLDtD+RnyZ5PMFckTC7B0k+TtuT+FGSr8cNjmxEy0geJbl3O9ddwGxGi0j24yhEwmaMieSuOAqRsBlDIjkWRyESNuOUSPrEUYiEzegTySlxFCJhM45FMiSOQiRsxl2RjImjEAmbsR9JizgKkbAZD5K8m3ZxFBcRyXNLL4DJ/TbJ93Jzn1VLX0jyaZK/NJ6bJP/NTShwlr6c5A+5OYn/k+TPaXf12M23DWhvP45ytIpkN982oL274mgVyW6+bUB7x+IYG8luvm1Ae/dTj2NoJLv5tgHtnRLHqZHs5tsGtDckjr6R7ObbBrQ3Jo5aJLv5tgHttYijK5LdfNuA9lrGcRjJbr5tQHtTxFGOn824D2jufpLfZ5o4dvNtA9oTB3QQB3QQB3SYMo6fz7gPaO5+kt9lujhW8TQgDCEO6CAO6CAO6CAOOOIHmSaONyMONuAqyVsRB3RqGYk42KQWkYiDTRsTiTi4CEMieRhxcEFOiUQcXKQ+kYiDi3YsEnFA7o5EHLBnPxJxwB2uknwn4gAAAAAAAAAAAAAAAAAAAG78D3RBZn+BbaFgAAAAAElFTkSuQmCC').GetIcon()
        self.SetIcon(icon)
        self.filestuff = ""
        self.key = 0
        self.filestatus = 0
        self.keystatus = 0
        self.mode = 0
        self.SetSize((400, 80))
        self.panel_1 = wx.Panel(self, wx.ID_ANY)
        self.combo_box_1 = wx.ComboBox(self.panel_1, wx.ID_ANY, choices=["Action...", "Encrypt", "Decrypt"], style=wx.CB_DROPDOWN | wx.CB_READONLY | wx.TE_PROCESS_ENTER)
        self.text_ctrl_1 = wx.TextCtrl(self.panel_1, wx.ID_ANY, "", style=wx.TE_NO_VSCROLL | wx.TE_PROCESS_ENTER)
        self.button_1 = wx.Button(self.panel_1, wx.ID_ANY, "OK")
        self.__set_properties()
        self.__do_layout()
        self.Bind(wx.EVT_COMBOBOX, self.oncomboboxchange, self.combo_box_1)
        self.Bind(wx.EVT_TEXT, self.ontextinputhandler, self.text_ctrl_1)
        self.Bind(wx.EVT_BUTTON, self.okbutton, self.button_1)

    def __set_properties(self):
        self.SetTitle("Juan Méndez' encryption tool")
        self.combo_box_1.SetSelection(0)
        self.button_1.Disable()
        self.text_ctrl_1.Disable()

    def __do_layout(self):
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_4 = wx.BoxSizer(wx.VERTICAL)
        sizer_3 = wx.BoxSizer(wx.VERTICAL)
        label_1 = wx.StaticText(self.panel_1, wx.ID_ANY, "Choose an action")
        sizer_3.Add(label_1, 0, wx.ALIGN_CENTER, 0)
        sizer_3.Add(self.combo_box_1, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALL | wx.EXPAND, 0)
        sizer_2.Add(sizer_3, 1, wx.EXPAND, 0)
        label_2 = wx.StaticText(self.panel_1, wx.ID_ANY, "Key:")
        sizer_4.Add(label_2, 0, wx.ALIGN_CENTER, 0)
        sizer_4.Add(self.text_ctrl_1, 0, wx.EXPAND, 0)
        sizer_2.Add(sizer_4, 1, wx.EXPAND, 0)
        sizer_2.Add(self.button_1, 0, wx.EXPAND, 0)
        self.panel_1.SetSizer(sizer_2)
        sizer_1.Add(self.panel_1, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade

    def oncomboboxchange(self, event):  # This function will trigger whenever the thing in te combobox changes. 
        if(not self.combo_box_1.GetStringSelection().startswith("Action")): 
            with wx.FileDialog(self, "Open file", wildcard="TXT files (*.txt)|*.txt",style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:
                if fileDialog.ShowModal() == wx.ID_CANCEL:  return
                else:   
                    try:
                        with open(fileDialog.GetPath(), "r", encoding='utf-8') as file:   #Open selected file
                            self.filestuff =  file.read()
                            print("[INFO] File has been opened successfully")
                            self.filestatus = 1
                            self.text_ctrl_1.Enable()
                            if(self.keystatus): self.button_1.Enable()
                            self.mode = self.combo_box_1.GetStringSelection()   #This can be either 'Decrypt' or 'Encrypt'
                            return
                    except Exception as e: print("[ERROR] Woah! There was a problem:",e)
        self.mode = 0
        self.button_1.Disable()
        self.text_ctrl_1.Disable()

    def ontextinputhandler(self, event):  # Validate key value 
        value = self.text_ctrl_1.GetValue()
        if(value.isdigit()):
            if(int(value)>0):   
                self.key = int(value) #%RING_KEY
                print("[INFO] Key now is",self.key)
                self.keystatus = 1
                if(self.filestatus):
                	self.button_1.Enable()
                return 
            else:
                wx.MessageBox('The key value must be an integer!', 'Error', wx.OK | wx.ICON_ERROR)
                self.text_ctrl_1.SetValue("");
        else:
            if(value==""):  pass
            else:
                wx.MessageBox('The key value must be an integer!', 'Error', wx.OK | wx.ICON_ERROR)
                self.text_ctrl_1.SetValue("");
        self.keystatus = 0
        self.button_1.Disable()

    def okbutton(self, event):  # Here, we do all the things we need to do whenever the OK button is pressed
        print("[INFO] KeyStatus:",self.keystatus,"FileStatus:",self.filestatus,"Mode:",self.mode,"Key:",self.key)
        #if(self.mode == 0):     #Return if no action has been selected
         #   wx.MessageBox("You must choose an action!", 'Error', wx.OK | wx.ICON_ERROR)
          #  return  
        if(self.keystatus and self.filestatus):
            with wx.FileDialog(self, "Save "+self.mode+"ed file", defaultFile=self.mode+"ed.txt", wildcard="TXT files (*.txt)|*.txt",
                       style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as fileDialog:
                if fileDialog.ShowModal() == wx.ID_CANCEL:  return
                try:
                    with open(fileDialog.GetPath(),"w", encoding="utf-8") as file:    #Try to create a new file
                        if(self.mode=="Decrypt"):
                            print("[DECRYPT]")
                            data, info = basicencryptdecrypt(self.key,self.filestuff)
                            written = file.write(data)
                        else:   
                            print("[ENCRYPT]")
                            data, info = basicencryptdecrypt(self.key,self.filestuff,"Fake love should be outdressed, and not because it's not a joke. You never know she is")
                            written = file.write(data)
                        if(not info):   wx.MessageBox(str(written)+" bytes written to disk.", self.mode+" OK",  wx.OK | wx.ICON_INFORMATION)
                        else:   wx.MessageBox(str(written)+" bytes written to disk. There were errors trying to decrypt your file ", self.mode+" failure",  wx.OK | wx.ICON_ERROR)
                except Exception as e: print("[ERROR] Woah! There was a problem:",e)
            self.filestatus = 0
        else:   
            wx.MessageBox("You must choose a file/Set a key!", 'Error', wx.OK | wx.ICON_ERROR)

class MyApp(wx.App):
    def OnInit(self):

        self.frame = MyFrame(None, wx.ID_ANY, "")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
