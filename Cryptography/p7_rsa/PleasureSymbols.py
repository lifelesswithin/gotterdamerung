#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import zlib, os

'''
Generación de un par de llaves usando el algoritmo RSA.

Parámetros:
privateKeyName  --  Nombre con el cual debería guardarse la llave privada.
publicKeyName  --  Nombre con el cual debería guardarse la llave pública.
'''

def GenerateKeyPair(privateKeyName,publicKeyName,keySize=2048):
    new_key = RSA.generate(keySize, e=65537)
    private_key = new_key.exportKey('PEM')
    public_key = new_key.publickey().exportKey('PEM')

    with open(privateKeyName,'wb') as privateKeyFile,   open(publicKeyName,'wb') as publicKeyFile:
        privateKeyFile.write(private_key)
        publicKeyFile.write(public_key)

class PleasureSymbol():
    def __init__(self,inputfile):
        with open(inputfile,'rb') as input_file:
            self.file = input_file.read()
            self.name = os.path.basename(inputfile)

    def GetName(self):  return self.name

    '''
    Cifrar un archivo usando RSA.

    Parámetros:
    outputName  -- Nombre para con el cual se debería guardar el archivo.
    publicKey   -- Archivo que contiene la llave pública.
    '''

    def EncryptFileWithRsa(self,outputName,publicKey):
        with open(publicKey, "rb") as pub_key:
            blob = encrypt_blob(self.file, pub_key.read())
            with open(outputName, "wb") as encrypted_file:
                encrypted_file.write(blob)

    '''
    Descifrar un archivo usando RSA.

    Parámetros:
    outputName  -- Nombre para con el cual se debería guardar el archivo.
    privateKey   -- Archivo que contiene la llave privada.
    '''

    def DecryptFileWithRsa(self,outputName,privateKey):
        with open(privateKey, "rb") as priv_key:
            decrypted_blob = decrypt_blob(self.file, priv_key.read())
            with open(outputName, "wb") as decrypted_file:
                decrypted_file.write(decrypted_blob)



def encrypt_blob(blob, public_key):
    '''
    Para determinar qué tan grande puede ser un chunk, se usa la longitud de la llave privada en bytes 
    y se le restan 42 bytes (PKCS1_OAEP). Cada chunk después se cifra individualmente.
    '''
    rsaKey = RSA.importKey(public_key)
    chunk_size = rsaKey.size_in_bytes() - 42
    rsaKey = PKCS1_OAEP.new(rsaKey)
    blob = zlib.compress(blob)    
    offset = 0
    endOfFile = False
    encrypted = bytes()
    blob_len  = len(blob)//chunk_size if(len(blob)>=chunk_size) else len(blob)
    encryptedCount = 0

    while not endOfFile:
        chunk = blob[offset:offset + chunk_size]

        '''
        Se llegó al final del archivo, pero este puede necesitar padding. Agregar bytes de sobra para 
        poder cifrar este chunk.
        '''

        if len(chunk) % chunk_size != 0:
            endOfFile = True
            length  =   (chunk_size - len(chunk)) % chunk_size
            chunk += bytes([length])*length

        print("Encrypted so far (%): ",(encryptedCount/blob_len)*100,end='\r')
        encrypted += rsaKey.encrypt(chunk)
        encryptedCount += 1
        offset += chunk_size
    return encrypted

def decrypt_blob(blob, private_key):  
    rsakey = RSA.importKey(private_key)
    chunk_size = rsakey.size_in_bytes()
    rsakey = PKCS1_OAEP.new(rsakey)
    offset = 0
    decrypted = bytes()
    blob_len  = len(blob)//chunk_size if(len(blob)>=chunk_size) else len(blob)
    decryptedCount = 0

    while offset < len(blob):
        chunk = blob[offset: offset + chunk_size]
        decrypted += rsakey.decrypt(chunk)
        offset += chunk_size
        decryptedCount += 1
        print("Decrypted so far (%): ",(decryptedCount/blob_len)*100,end='\r')

    return zlib.decompress(decrypted)

if __name__ == "__main__":
    
    kSize = 2048
    GenerateKeyPair('private_key.pem','public_key.pem',kSize)

    a = PleasureSymbol('sabrosongo')
    a.EncryptFileWithRsa('Encrypted.txt','public_key.pem')

    b = PleasureSymbol('Encrypted.txt')
    b.DecryptFileWithRsa('decrypted.txt','private_key.pem')
