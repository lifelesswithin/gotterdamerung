#!/usr/bin/env python
# -*- coding: UTF-8 -*-
########################################################################################################################
#
# ESCUELA SUPERIOR DE CÓMPUTO
# Juan Manuel Dávila Méndez
# Python app to encrypt and decrypt images using AES and DES in ECB mode.
# Made with <3 and FourLoko
#
# ######################################################################################################################

import wx, logging
from wx.lib.embeddedimage import PyEmbeddedImage
from NiceBackend import DisemboweledImage

class P5(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: P5.__init__
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.SetSize((550, 100))
        icon = PyEmbeddedImage('iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABghJREFUeJzt3U+rJFcdx+HvjYsgd+K4UONCkCxD3kAYcGFwqW/AF6Cg2bjIUmkzWWSRd5OAu+ig+AeEcRTdqG9As0jIGFwExsW9xzSdW32qq0796erngYLbcPlxTlMfalPVlQAAAAAAAAAAAAAAAAAAAAAAAAAAAFyEqyT3Gs164XYebMJVkneSPEnylZGzXkzytyRvRSRsQInj2e0xJpISR5klEs7aYRxjIjmMQyScta44hkTSFYdIOEu1OE6JpBaHSDgrfePoE0nfOETCWTg1jmORnBqHSFi1oXHcFcnQOETCKo2NYz+SlzMuDpGwKq3ieJbkgyT/bDRLJCxuzXGIhEWdQxwiYRHnFIdImNU5xiESZnHOcYiESW0hDpEwiS3FIRKa2mIcIqGJLcchEka5hDhEwiAt4/hXkp82miUSFtc6jldu577RaKZIWMxUcRRrj+RRkueHfnls29RxFGuN5FHa/W4XGzNXHMXaIhEHneaOo1hLJOKg01JxFEtHIg46LR1HsVQk4qDTWuIo5o5EHHRaWxzFXJGIg05rjaOYOhJx0Kn1vVXfn2idU0XyOMk3J1ozZ651HH9P8jTJtyZab+tIniT5JG3eT8IGvZTk47SLo3w+h0hKHPufRcLnPMi4SA7jOIdIDuMQCUcNjaQrjjVH0hWHSDjq1EhqcawxklocIuGovpF8kOQfPf5vTZH0jUMkHFWL5NQ41hDJqXGIhKO6Ihkax5KRDI1DJBx1GMnYOJaIZGwc5fhTPFHIHUokreKYM5JWcTxL8vpEa2UDvpu2ccwRycOIgxm8mOSvaR/HlJF8O+JgBlPHMUUk4mAWc8XRMhJxMIu542gRiTiYxdj3kC8RiTiYxdJxDInktYiDGawljlMiEQezWFscfSJpGcePB3xnXIivZZ1xHIvElWPDnlt6AQc+SfLvpRdxxHWSX+SzSF5L8l6SLzaY/Zsk34jXGlBxLzc/e7P01aJ2Jflh2l05fr3399sRCRVrj+RxpolDJPS21kimjkMk9La2SOaKQyT0tpZI5o5DJPR2nWUjWSoOkdDbUpEsHYdI6G3uSNYSh0joba5I1haHSOht6kjWGodI6O06ya+y7jh+kptbSKaIWCRUtY6kZRzlrtwXIhIW1CqS93PzNOBHDWb96GCNImFRYyN5/3ZGkryacZEcxlGIhEUNjWQ/jmJoJF1xFCJhUddJfplxcRSnRlKLoxAJi+obybE4ir6R9I2jEAmLqkXSJ46iFsmpcRQiYVFdkZwSR9EVydA4CpGwqMNIhsRRHEYyNo5CJCyqRDImjuLVJB+mXRyFSFjUdcbHUXy10ZxDIoEKkUCFSKBCJFAhEqgQCVSIBCpEAhUigQqRQIVIoEIkUCESqBAJVIgEKkQCFSKBCpFAhUigQiRQIRKoEAlUfCnJHyMS+JyrJO8keZrkSUQC/1fiKCeySODWYRwigVtdcYiEi1eLQyRcrL5xiISLc2ocIuFiDI1DJGze2DhEwma1ikMkbNLDtD+RnyZ5PMFckTC7B0k+TtuT+FGSr8cNjmxEy0geJbl3O9ddwGxGi0j24yhEwmaMieSuOAqRsBlDIjkWRyESNuOUSPrEUYiEzegTySlxFCJhM45FMiSOQiRsxl2RjImjEAmbsR9JizgKkbAZD5K8m3ZxFBcRyXNLL4DJ/TbJ93Jzn1VLX0jyaZK/NJ6bJP/NTShwlr6c5A+5OYn/k+TPaXf12M23DWhvP45ytIpkN982oL274mgVyW6+bUB7x+IYG8luvm1Ae/dTj2NoJLv5tgHtnRLHqZHs5tsGtDckjr6R7ObbBrQ3Jo5aJLv5tgHttYijK5LdfNuA9lrGcRjJbr5tQHtTxFGOn824D2jufpLfZ5o4dvNtA9oTB3QQB3QQB3SYMo6fz7gPaO5+kt9lujhW8TQgDCEO6CAO6CAO6CAOOOIHmSaONyMONuAqyVsRB3RqGYk42KQWkYiDTRsTiTi4CEMieRhxcEFOiUQcXKQ+kYiDi3YsEnFA7o5EHLBnPxJxwB2uknwn4gAAAAAAAAAAAAAAAAAAAG78D3RBZn+BbaFgAAAAAElFTkSuQmCC').GetIcon()
        self.SetIcon(icon)
        self.cipher_box = wx.ComboBox(self, wx.ID_ANY, choices=["AES", "DES"], style=wx.CB_DROPDOWN | wx.CB_READONLY)
        self.cipher_mode_box = wx.ComboBox(self, wx.ID_ANY, choices=["ECB", "OFB", "CFB", "CBC"], style=wx.CB_DROPDOWN | wx.CB_READONLY)
        self.action_box = wx.ComboBox(self, wx.ID_ANY, choices=["Open...", "Encrypt", "Decrypt"], style=wx.CB_DROPDOWN | wx.CB_READONLY)
        self.key_text_ctrl = wx.TextCtrl(self, wx.ID_ANY, "", style=wx.TE_NO_VSCROLL)
        self.iv_text_ctrl = wx.TextCtrl(self, wx.ID_ANY, "", style=wx.TE_NO_VSCROLL)
        self.button_1 = wx.Button(self, wx.ID_ANY, "OK")

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_COMBOBOX, self.OnCipherModeChange, self.cipher_mode_box)
        self.Bind(wx.EVT_COMBOBOX, self.OnModeChange, self.action_box)
        self.Bind(wx.EVT_TEXT, self.OnKeycHange, self.key_text_ctrl)
        self.Bind(wx.EVT_TEXT, self.OnIVChange, self.iv_text_ctrl)
        self.Bind(wx.EVT_BUTTON, self.OnOkPress, self.button_1)
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: P5.__set_properties
        self.SetTitle("AES and DES image modifier")
        self.cipher_mode_box.SetSelection(0)
        self.cipher_box.SetSelection(0)
        self.action_box.SetSelection(0)
        self.key_text_ctrl.Disable()
        self.iv_text_ctrl.Disable()
        self.button_1.Disable()
        # end wxGlade
        self.imageFile   = ""

    def __do_layout(self):
        # begin wxGlade: P5.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_4 = wx.BoxSizer(wx.VERTICAL)
        sizer_5 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_3 = wx.BoxSizer(wx.VERTICAL)
        sizer_6 = wx.BoxSizer(wx.HORIZONTAL)
        
        sizer_6.Add(self.cipher_box, 1, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
        sizer_6.Add(self.cipher_mode_box, 1, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
        sizer_3.Add(sizer_6, 1, wx.EXPAND, 0)

        #sizer_3.Add(self.cipher_box, 1, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
        sizer_3.Add(self.action_box, 1, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
        sizer_2.Add(sizer_3, 16, wx.EXPAND, 0)
        label_1 = wx.StaticText(self, wx.ID_ANY, "Key :\t\t\t\t\t IV:")
        sizer_4.Add(label_1, 1, wx.ALIGN_CENTER | wx.ALL, 0)
        sizer_5.Add(self.key_text_ctrl, 1, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
        sizer_5.Add(self.iv_text_ctrl, 1, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
        sizer_4.Add(sizer_5, 1, wx.EXPAND, 0)
        sizer_2.Add(sizer_4, 29, wx.ALL | wx.EXPAND, 0)
        sizer_2.Add(self.button_1, 0, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
        sizer_1.Add(sizer_2, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade

    def Reset(self):
        self.action_box.SetSelection(0)
        self.key_text_ctrl.Disable()
        self.iv_text_ctrl.Disable()
        self.button_1.Disable()

    def OnCipherModeChange(self, event):  
        if(self.cipher_mode_box.GetValue()=='ECB'):     
            self.iv_text_ctrl.Disable()
            self.iv_text_ctrl.SetValue('')
            return 
        else:
            if(self.imageFile and self.action_box.GetSelection()!=0):                         
                self.iv_text_ctrl.Enable()
                self.key_text_ctrl.Enable()
                if(not len(self.iv_text_ctrl.GetValue())>0):
                    self.button_1.Disable()
                return 
        self.button_1.Disable()


    def OnModeChange(self, event):  # wxGlade: P5.<event_handler>
        if(not self.action_box.GetStringSelection().startswith("Open")): 
            with wx.FileDialog(self, "Open file", wildcard="BMP files (*.bmp, *.BMP)|*.bmp;*.BMP",style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:
                if fileDialog.ShowModal() == wx.ID_CANCEL:  
                    self.key_text_ctrl.Disable()
                    self.action_box.SetSelection(0)
                    return
                else:   
                    try:
                        self.imageFile = DisemboweledImage(fileDialog.GetPath())
                        self.key_text_ctrl.Enable()
                        if(self.cipher_mode_box.GetValue()!='ECB'):    
                            self.iv_text_ctrl.Enable()
                            self.key_text_ctrl.Enable()
                        if(len(self.key_text_ctrl.GetValue())>0 and self.cipher_mode_box.GetValue()=='ECB'):    self.button_1.Enable()
                        if(self.cipher_mode_box.GetValue()!='ECB' and len(self.key_text_ctrl.GetValue())>0 and len(self.iv_text_ctrl.GetValue())>0): self.button_1.Enable()
                        return
                    except Exception as e: 
                    	wx.MessageBox("Your file doesn't seem to be a valid image. Error:\n\n"+str(e), "Hell no!",  wx.OK | wx.ICON_ERROR)  
                    	self.action_box.SetSelection(0)
        else:	self.key_text_ctrl.Disable()

    def OnIVChange(self,event):
        if(len(self.iv_text_ctrl.GetValue())>0 and len(self.key_text_ctrl.GetValue())>0):
            self.button_1.Enable()
            return
        self.button_1.Disable()

    def OnKeycHange(self, event):  # wxGlade: P5.<event_handler>
        if(len(self.key_text_ctrl.GetValue())>0):                                                 #Key field
            if(self.cipher_box.GetValue()=='DES' and len(self.key_text_ctrl.GetValue())>8):      #DES Mode and Key field
                wx.MessageBox("Key should be at most 8 characters long if you want to use DES-ECB mode", "Hell no!",  wx.OK | wx.ICON_ERROR)  
                self.key_text_ctrl.SetValue(self.key_text_ctrl.GetValue()[:8])                      #Set field value to its contents but limited to 8 chars
            if(self.cipher_mode_box.GetValue()=='ECB'):                        self.button_1.Enable()
            else:
                if(len(self.iv_text_ctrl.GetValue())>0):            self.button_1.Enable()
        else:	self.button_1.Disable()
        

    def OnOkPress(self, event):  # wxGlade: P5.<event_handler>
        with wx.FileDialog(self, 
            "Save modified image", 
            defaultFile= self.cipher_box.GetValue() +'_' + self.cipher_mode_box.GetValue() + '_' + self.action_box.GetValue() + 'ed_IMAGE.bmp',
            wildcard="BMP files (*.bmp)|*.bmp",
            style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as outputFile:
                if outputFile.ShowModal() == wx.ID_CANCEL:  return
                else:
                    try:
                        if(self.action_box.GetValue() == "Encrypt"):
                            if(self.cipher_box.GetValue()=='AES'):      self.imageFile.SaveAesModifiedImage(outputFile.GetPath(),self.key_text_ctrl.GetValue(),self.cipher_mode_box.GetValue(),'ENCRYPT',self.iv_text_ctrl.GetValue())
                            else:                                       self.imageFile.SaveDesModifiedImage(outputFile.GetPath(),self.key_text_ctrl.GetValue(),self.cipher_mode_box.GetValue(),'ENCRYPT',self.iv_text_ctrl.GetValue())
                        else:
                            if(self.cipher_box.GetValue()=='AES'):      self.imageFile.SaveAesModifiedImage(outputFile.GetPath(),self.key_text_ctrl.GetValue(),self.cipher_mode_box.GetValue(),'DECRYPT',self.iv_text_ctrl.GetValue())
                            else:                                       self.imageFile.SaveDesModifiedImage(outputFile.GetPath(),self.key_text_ctrl.GetValue(),self.cipher_mode_box.GetValue(),'DECRYPT',self.iv_text_ctrl.GetValue())
                        wx.MessageBox("Image saved successfully", "Hell yes!",  wx.OK | wx.ICON_INFORMATION)  
                    except Exception as e:                  wx.MessageBox("Something wrong happened!"+str(e), "Hell no!",  wx.OK | wx.ICON_ERROR)  
        self.Reset()                    

class MyApp(wx.App):
    def OnInit(self):
        self.frame = P5(None, wx.ID_ANY, "")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    app = MyApp(0)
    app.MainLoop()
