#!/usr/bin/env python

import socket, pickle, struct, sys, hashlib, time, datetime, os, glob
from multiprocessing import Process
from GetSizClass import *

class Chunk:
	def __init__( self, name, total, _bytes, part, md5file): 
		self.part = part
		self.total = total
		self.bytes = _bytes
		self.checksumfile = md5file
		self.name = name
		m = hashlib.md5()
		m.update( self.bytes )
		self.checksum = m.hexdigest()

def client():
	MCAST_GRP = '224.0.0.1'
	MCAST_PORT = 10000
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
	try:
		sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	except AttributeError:
		print("Error")
		sys.exit(-1)
	sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32) 
	sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)

	sock.bind((MCAST_GRP, MCAST_PORT))
	group = socket.inet_aton(MCAST_GRP)
	mreq = struct.pack('4sL', group, socket.INADDR_ANY)	#4stringLetter 
	sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
	
	bandera_1 = False
	incomplete = False
	incomplete_number = 0
	part_now = 0
	max_buffer_size = 4096
	
	now = datetime.datetime.now()
	name = str( now ) + "recibido"
	file = open( name ,'wb' )
	counter = 0

	print("esperando archivos...")
	while 1:
		info = sock.recv( 5000 )
		if info:
			data = pickle.loads( info )
	
			if data.part == 0:
				bandera_1 = True

			if( part_now == data.total ):
				file.close()
				os.rename(name,data.name)
				if(str(data.checksumfile)==str(md5(data.name))):
					print("\nmd5:",md5(data.name))
					print("\nArchivo recibido correctamente")
				else:
					os.remove(data.name)
					print("\nVerificación de integridad fallida, reintentando...")
					Process(target=client).start()
				break
			if( bandera_1 ):
				if( incomplete & incomplete_number == data.part ):
					incomplete = False
				
				if( not incomplete ):
					part_now = data.part
					total = data.total
					m = hashlib.md5()
					m.update( data.bytes )
					counter	+=  max_buffer_size

					if( data.checksum != m.hexdigest() ):
						print( "Error en la transmisión del archivo" )
						incomplete_number = part_now
						incomplete = True;
					else:
						#print( '%.2f%%' % ( float( data.part ) / float( total ) * 100 ), end='\r' )
						print_progress(int(data.part),int(total), prefix='', suffix="recibiendo "+data.name+"...", decimals=1, bar_length=50)
						file.write( data.bytes )
						part_now = data.part

if __name__ == '__main__':
	try:
		for fl in glob.glob("*recibido"):
			os.remove(fl)
		Process(target=client).start()
	except:
		sys.exit(0)