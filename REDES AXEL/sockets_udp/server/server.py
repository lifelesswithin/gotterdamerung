#!/usr/bin/env python

import socket, pickle, binascii, math, sys, time, hashlib, os
from GetSizClass import *

class Chunk:
	def __init__( self, name, total, _bytes, part, md5file): 
		self.part = part
		self.total = total
		self.bytes = _bytes
		self.checksumfile = md5file
		self.name = name
		m = hashlib.md5()
		m.update( self.bytes )
		self.checksum = m.hexdigest()

def main():
	if(len(sys.argv)<2):
		print("Uso: ",sys.argv[0],"'archivo a enviar'")
		sys.exit(0)
	else:
		targetfile = sys.argv[1]
		md5archivo = md5(targetfile)
		print("BROADCAST:  [",sys.argv[1],"]  MD5:  [",md5archivo,"]",sep="")
	MCAST_GRP = '224.0.0.1'
	MCAST_PORT = 10000
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
	sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 255)
	max_buffer_size = 4096
	while(True):
		file = open(targetfile, 'rb')
		size = os.path.getsize(targetfile)
		total = math.ceil( size / max_buffer_size )
		#print( "total: ", total )
		counter = 0
		i = 0
			#print( '%.2f%% ' % ( float( i ) / float( total ) * 100 ), end='\r' )
		while(True):
			bytes_read = file.read( max_buffer_size )
			chunk = Chunk( file.name, total, bytes_read, i,md5archivo)
			sock.sendto( pickle.dumps( chunk ), (MCAST_GRP, MCAST_PORT) )
			counter += max_buffer_size
			i += 1	
			#print(round((i/total)*100,4),"%", get_size(chunk),"i_",i,"t_",total,end='\r' )
			print_progress(i, total, prefix='', suffix='enviando...', decimals=1, bar_length=50)
			if (not bytes_read):
				break
		file.close()
		time.sleep(1)
	sock.close()



if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print("\nRIP")
		sys.exit(0)