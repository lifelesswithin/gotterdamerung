#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import wx, os, sys, threading, socket, time, pickle, multiprocessing, shutil, hashlib
from threading import Thread
from multiprocessing import Process, Value, Lock, Manager
etiquetasTxt = []
CarpetaDelServidor = "Archivos_Servidor"
puertoChidori = 6669
puertoChidori2 = 6699
puertoChidori3 = 6969
status = None
term = 1
RESP_ERROR = "error"
RESP_OK = "ok"
servidorIniciado = 0

def Archivo2ByteArray(filepath):        #función que toma un archivo y lo convierte a un bytearray
    datillos = b''
    if((getSize(filepath)/1000**2)>500):    #500MB
        chunkSize = 2**27   #Trozos de ~134MB
    else:   chunkSize = 2**22   #Trozos de ~4MB
    transferidos = 0
    f = open(filepath,'rb')
    l = f.read(chunkSize)
    while (l):
        transferidos += sys.getsizeof(l)
        print("leyendo... ",round(((transferidos*100)/getSize(filepath)),1)," % \r", sep=' ', end='')
        datillos += l
        l = f.read(chunkSize)
    f.close()
    return datillos

def ByteArray2Archivo(filename,bytearr):    #fumción que escribe un arreglo de bytes a un archivo
    nuevoArchivo = open(filename, "wb")
    ArregloBytesChido = bytearray(bytearr)
    nuevoArchivo.write(ArregloBytesChido)
    nuevoArchivo.close()

def getSize(filename):      #función para obtener el tamaño de un archivo
    st = os.stat(filename)  #probar si esta el archivo
    return st.st_size       #checar tam

class Datillo():
    def __init__(self,dato,destinatario,nombreArchivo):
        self.dato = dato
        self.destinatario = destinatario
        self.nombreArchivo = nombreArchivo
        self.tamArchivo = sys.getsizeof(self.dato)/(1000**2)
        sha_1 = hashlib.sha1()
        sha_1.update(dato)
        self.hash = sha_1.hexdigest()

class clientes_num(object):     #comunicacion entre el proceso de envío
    def __init__(self, initval=0):  self.val = Value('i', initval)    #dato primitivo
        #self.lock = Lock()
    def increment(self):    self.val.value += 1
        #with self.lock:
    def decrement(self):    self.val.value -= 1
        #with self.lock:
    def value(self):    return self.val.value
        #with self.lock:

def recvall(sock,unpickle=None,buffsiz=None):        # Funcion que se encarga de recibir datos a traves de un socket sin importar su tamaño
    if(buffsiz):            #en caso de que se desee recibir datos grandes
        BUFF_SIZE = 2**buffsiz    #18 es recomendado.
    else:   BUFF_SIZE = 512    # Chunks de 512B
    data = b''          # data = arreglo de bytes
    while (True):           # loop infinito
        part = sock.recv(BUFF_SIZE) #recibir de 512B en 512B
        data += part                #anexar los datos
        if(buffsiz):
            print("[I]  Recibiendo de ",sock.getpeername(),"... ",round(sys.getsizeof(data)/1000**2,2)," MB\r",end="",sep="")
        if (len(part) < BUFF_SIZE): #Terminar cuando sea 0 o ya no hayan datos
            break                       #-->break
    if(unpickle):
        if(data):   return pickle.loads(data)
        else:   return None
    else:   return data

class ServidorThreaded3():      #servicio de recepción de archivos
    def __init__(self):
        self.host = socket.gethostname()
        self.port = puertoChidori3
        self.status3 = -1
        self.cwd = os.path.dirname(os.path.realpath(__file__))
        self.sock3 = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #TCP
        self.sock3.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #REUSAR ADDR
        try:
            self.sock3.bind((self.host,self.port))
            self.status3 = 0
        except: print("[E]  Error al arrancar Servidor3")

    def listen3(self):
        if(self.status3==0):
            self.sock3.listen(self.port)
            print("[I]  Servidor3 iniciado exitosamente")
            while True:
                client, address = self.sock3.accept()    #aceptar conexiones
                print("[I]  S3 recibida conexión:",client.getpeername())
                threading.Thread(target = self.escucharCliente3, args = (client,address)).start() #thread
        else:   print("[W]  S3 Imposible iniciar el servidor...")

    def escucharCliente3(self, client, address):
        while True:
            try:
                data = recvall(client)      #chunks de .5MB
                if data:
                    recibido = data.decode("utf-8")
                    print("[I] solicitud de subida:",recibido,"de",client.getpeername())
                    #try:
                    #    os.chdir(CarpetaDelServidor)
                    #except: print("[!]S3 no hay carpeta del servidor")
                    path = self.cwd + "/" + CarpetaDelServidor + "/" + recibido
                    print("Nom:",path,getSize(path))
                    try:    datillos = Archivo2ByteArray(path)
                    except: print("error bytearray")
                    try:
                        print(">>>",sys.getsizeof(datillos),os.path.basename(path))
                        ayyylmao = Datillo(datillos,"ayyylmao",os.path.basename(path))
                    except: print("Error en la clase")
                    print(ayyylmao.nombreArchivo,"---",ayyylmao.tamArchivo)
                    try:
                        client.send(pickle.dumps(ayyylmao)) #enviar datos
                        print("enviado>",ayyylmao.nombreArchivo,ayyylmao.tamArchivo)
                    except: print(client.getpeername(),"[W]   Tiene problemas para recibir.")
                else: #if not data
                    raise ValueError('Client disconnected')
            except:     #si el cliente cierra la conexión
                print("[I]S3  desconectado: ",client.getpeername(),sep="")
                client.close()                          #cerrar socket
                break                                   #matar el hilo

class ServidorThreaded2():      #servicio de recepción de archivos
    def __init__(self,port,listaArchivos):
        self.host = socket.gethostname()
        self.port = puertoChidori2
        self.listaArchivos = listaArchivos
        self.status2 = -1
        self.cwd = os.getcwd()
        self.sock2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #TCP
        self.sock2.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #REUSAR ADDR
        try:
            self.sock2.bind((self.host,self.port))
            self.status2 = 0
        except: print("[E]  Error al arrancar Servidor2")
        try:    os.mkdir(CarpetaDelServidor)
        except:
            shutil.rmtree(CarpetaDelServidor, ignore_errors=True)
            print("[I]  Limpiando directorio de descargas")
            os.mkdir(CarpetaDelServidor)

    def listen2(self):
        if(self.status2==0):
            self.sock2.listen(self.port)
            print("[I]  Servidor2 iniciado exitosamente")
            while True:
                client, address = self.sock2.accept()    #aceptar conexiones
                print("[I]  S2 recibida conexión:",client.getpeername())
                threading.Thread(target = self.escucharCliente2, args = (client,address)).start() #thread
        else:   print("[W]  S2  Imposible iniciar el servidor...")

    def escucharCliente2(self, client, address):
        while True:
            try:
                data = recvall(client,"",14)      #chunks de .5MB
                if data:
                    recibido = pickle.loads(data)
                    os.chdir(CarpetaDelServidor)
                    ByteArray2Archivo(recibido.nombreArchivo,recibido.dato)
                    self.listaArchivos.update({recibido.nombreArchivo:[recibido.destinatario,recibido.tamArchivo]})
                    print("\n\nLista de archivos ahora:",self.listaArchivos)
                    try:    del recibido          #Eliminar variable para liberar memoria
                    except: pass
                    os.chdir(self.cwd)
                else: #if not data
                    raise ValueError('Client disconnected')
            except:     #si el cliente cierra la conexión
                print("[I]  S2  desconectado: ",client.getpeername(),sep="")
                client.close()                          #cerrar socket
                break                                   #matar el hilo

class ServidorThreaded():                   #servidor principal para recepción y envío de mensajes
    def __init__(self, port, cont,listaArchivos):
        self.host = socket.gethostname()    #para que se puedan aceptar conexiones desde afuera
        self.port = port        #puerto
        self.cont = cont        #contador de conexiones
        self.listaArchivos = listaArchivos
        self.clientesConectados = []    #lista con los sockets de los clientes conectados
        self.listaClientes = {}         #diccionario con las entradas Socket:NombreUsuario
        self.cwd = os.getcwd()
        self.status = -1                #estado de ejecución (flag)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #TCP
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #REUSAR ADDR
        try:
            self.sock.bind((self.host, self.port))  #tratar de asociar el puerto
            self.status = 0                         #seguir
        except socket.error:
            self.status = -1                        #RIP el server

    def listen(self):
        if(self.status==0):
            self.sock.listen(self.port)                 #escuchar en el puerto
            print("[I]  Servidor iniciado exitosamente")
            while True:
                client, address = self.sock.accept()    #aceptar conexiones
                self.cont.increment()                   #+1 cont. conexiones
                self.clientesConectados.append(client)  #agregar este cliente
                print("[I]  Conexión recibida:",client.getpeername())
                threading.Thread(target = self.escucharCliente, args = (client,address)).start() #thread
        else:   print("[W]  Imposible iniciar el servidor...")

    def escucharCliente(self, client, address):
        while True:
            try:
                data = recvall(client,1)      #recibir todos los datos de cualquier cliente (raw)
                unpickled = data
                if data:                    #si se reciben datos 0-comando(msj/peerlist/upld/dwlnd, 1-NombreEmisor, 2-mensaje, 3-privado/destino
                   # unpickled = pickle.loads(data)      #des-serializar los datos
                    if(unpickled[0]=="login"):          #conexión en modo login (ejecutada al principio del programa)
                        noretry=0
                        for entrada in self.listaClientes:  #buscar en todas las conexiones algún nombre de usuario
                            if(self.listaClientes[entrada]==unpickled[1]):  #{idConexiónIP/PORT:NombreUsuario}==NombreUsuario
                                print("[W/LOGIN]    Cliente repetido, rechazando...")
                                client.send(RESP_ERROR.encode())
                                noretry = 1     #este flag sirve para
                        if(noretry!=1):         #no tratar de hacer un client.send() en la parte posterior (el cliente cierra el socket)
                            print("[I/LOGIN]",unpickled[1],"es válido")
                            client.send(RESP_OK.encode())  #codificar y enviar
                            loginflag = 1                   #no eliminar este cliente inmediatamente
                    elif(unpickled[0]=="agregarNombre"):    self.listaClientes.update({str(client.getpeername()):unpickled[1]})
                    elif(unpickled[0]=="msj"):                #conexión en modo de envío de mensajes
                        for clientecillo in (self.clientesConectados):  #enviar a cada cliente
                            if(clientecillo!=client):       #evitar enviar al cliente que emite el mensaje
                                print("[I]",client.getpeername(),"––>",clientecillo.getpeername())
                                try:    clientecillo.send(pickle.dumps(data)) #enviar datos
                                except: print(clientecillo.getpeername(),"[W]   Tiene problemas para recibir.")
                    elif(unpickled[0]=="listaClientes"):
                        listaPeersAmigable = []
                        #print("[I] solicita la lista de clientes",client.getpeername())
                        for entrada in self.listaClientes:
                            listaPeersAmigable.append(self.listaClientes[entrada])
                        try:    client.send(pickle.dumps(["listaClientes",listaPeersAmigable,"",""]))
                        except: print(client.getpeername,"[W]   Tiene problemas para recibir.")
                    elif(unpickled[0]=="listaArchivos"):
                        #print(["listaArchivos",list(self.listaArchivos.items()),"",""],"[I] solicita la lista de archivos",client.getpeername())
                        try:    client.send(pickle.dumps(["listaArchivos",list(self.listaArchivos.items()),"",""]))
                        except: print(client.getpeername,"[W]   Tiene problemas para recibir.")
                else: #if not data
                    raise ValueError('Client disconnected')
            except:     #si el cliente cierra la conexión
                print("[!]  Desconectado: ",client.getpeername()," (",self.cont.value(),") conectados",sep="")
                if(str(client.getpeername()) in self.listaClientes):    #al desconectar, quitar el cliente
                        print("[!]  Removiendo de la lista",client.getpeername())   #del diccionario Cliente:Nombre
                        self.listaClientes.pop(str(client.getpeername()))
                self.clientesConectados.remove(client)  #remover al cliente de la lista de sockets
                self.cont.decrement()                   #disminuir contador
                client.close()                          #cerrar socket
                break                                   #matar el hilo


class ProcesoServidor(object):
    def __init__(self,cont,listaArchivos):
        self.servidorsillo = ServidorThreaded(puertoChidori,cont,listaArchivos).listen()

class ProcesoServidor2(object):
    def __init__(self,listaArchivos):
        self.servidorsillo2 = ServidorThreaded2(puertoChidori2,listaArchivos).listen2()

class ProcesoServidor3(object):
    def __init__(self):
        self.servidorsillo3 = ServidorThreaded3().listen3()


class IniciadoAnim(object):
    def __init__(self,cont):
        self.counter = cont
        while True:
            if(term==0):
                wx.CallAfter(etiquetasTxt[0].SetLabel, "            Estado: INICIADO")
                wx.CallAfter(etiquetasTxt[1].SetLabel, "        " + str(self.counter.value()) + " cliente(s) conectados")
                time.sleep(0.5)
                wx.CallAfter(etiquetasTxt[0].SetLabel, "            Estado: INICIADO.")
                wx.CallAfter(etiquetasTxt[1].SetLabel, "        " + str(self.counter.value()) + " cliente(s) conectados")
                time.sleep(0.5)
                wx.CallAfter(etiquetasTxt[0].SetLabel, "            Estado: INICIADO..")
                wx.CallAfter(etiquetasTxt[1].SetLabel, "        " + str(self.counter.value()) + " cliente(s) conectados")
                time.sleep(0.5)
            elif(term==1):
                wx.CallAfter(etiquetasTxt[0].SetLabel, "            Estado: DETENIDO")
                wx.CallAfter(etiquetasTxt[1].SetLabel,"         0 cliente(s) conectados")
                break


class ventPrincipal(wx.Frame):
    def __init__(self, *args, **kwds):
        # Variables utilizadas para el funcionamiento
        self.mgr = multiprocessing.Manager()
        self.listaArchivos = self.mgr.dict()
        self.EstadoEjecucion = 0
        self.status = None  ##########
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.SetSize((382, 96))
        self.window_1 = wx.SplitterWindow(self, wx.ID_ANY)
        self.window_1_pane_1 = wx.Panel(self.window_1, wx.ID_ANY)
        self.window_2 = wx.SplitterWindow(self.window_1_pane_1, wx.ID_ANY)
        self.window_2_pane_1 = wx.Panel(self.window_2, wx.ID_ANY)
        self.button_1 = wx.Button(self.window_2_pane_1, wx.ID_ANY, "INICIAR SERVICIO")
        self.window_2_pane_2 = wx.Panel(self.window_2, wx.ID_ANY)
        self.button_2 = wx.Button(self.window_2_pane_2, wx.ID_ANY, "DETENER SERVICIO")
        self.window_1_pane_2 = wx.Panel(self.window_1, wx.ID_ANY)
        self.window_3 = wx.SplitterWindow(self.window_1_pane_2, wx.ID_ANY)
        self.window_3_pane_1 = wx.Panel(self.window_3, wx.ID_ANY)
        self.window_3_pane_2 = wx.Panel(self.window_3, wx.ID_ANY)

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_BUTTON, self.iniciar_srv, self.button_1)
        self.Bind(wx.EVT_BUTTON, self.detener_srv, self.button_2)
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: ventPrincipal.__set_properties
        self.SetTitle("Servidor multichat ("+ str(socket.gethostname())+":"+str(puertoChidori)+")")
        self.window_2.SetMinimumPaneSize(20)
        self.window_3.SetMinimumPaneSize(20)
        self.window_1.SetMinimumPaneSize(20)
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: ventPrincipal.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_3 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_7 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_6 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_5 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_4 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_4.Add(self.button_1, 1, wx.EXPAND, 0)
        self.window_2_pane_1.SetSizer(sizer_4)
        sizer_5.Add(self.button_2, 1, wx.EXPAND, 0)
        self.window_2_pane_2.SetSizer(sizer_5)
        self.window_2.SplitVertically(self.window_2_pane_1, self.window_2_pane_2, 180)
        sizer_2.Add(self.window_2, 1, wx.EXPAND, 0)
        self.window_1_pane_1.SetSizer(sizer_2)
        label_1 = wx.StaticText(self.window_3_pane_1, wx.ID_ANY, "  Estado: DETENIDO", style=wx.ALIGN_CENTER)
        sizer_6.Add(label_1, 1, wx.ALIGN_CENTER | wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 2)
        self.window_3_pane_1.SetSizer(sizer_6)
        label_2 = wx.StaticText(self.window_3_pane_2, wx.ID_ANY, "  0 cliente(s) conectados", style=wx.ALIGN_CENTER)
        sizer_7.Add(label_2, 1, wx.ALIGN_BOTTOM | wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 2)
        self.window_3_pane_2.SetSizer(sizer_7)
        self.window_3.SplitVertically(self.window_3_pane_1, self.window_3_pane_2, 180)
        sizer_3.Add(self.window_3, 1, wx.EXPAND, 0)
        self.window_1_pane_2.SetSizer(sizer_3)
        self.window_1.SplitHorizontally(self.window_1_pane_1, self.window_1_pane_2, 32)
        sizer_1.Add(self.window_1, 1, wx.EXPAND, 0)
        global etiquetasTxt
        for widgetX,widgetY in zip(self.window_3_pane_1.GetChildren(),self.window_3_pane_2.GetChildren()):
            if isinstance(widgetX, wx.StaticText):
                etiquetasTxt.append(widgetX)
            if isinstance(widgetY, wx.StaticText):
                etiquetasTxt.append(widgetY)
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade

    def iniciar_srv(self, event):  # wxGlade: ventPrincipal.<event_handler>
        global term
        term = 0
        contador = clientes_num(0)  #iniciar clase del contador para IPC (UI-proceso servidor)
        if(term!=1):
            self.proc1 = Process(target=ProcesoServidor,args=(contador,self.listaArchivos)).start()    #Proceso del servidor0
            self.proc2 = Process(target=ProcesoServidor2,args=(self.listaArchivos,)).start()    #Proceso del servidor0
            self.proc3 = Process(target=ProcesoServidor3).start()    #Proceso del servidor0
            self.ActualizarEstado = Thread(target=IniciadoAnim,args=(contador,)).start() #Hilo que actualiza el status del server

    def detener_srv(self, event):  # wxGlade: ventPrincipal.<event_handler>
        print(self.listaArchivos)
        try:
            self.proc1.terminate()                                               #Terminar el proceso del servidor
            self.proc2.terminate()
        except: print("servidor ya está detenido")
        global term
        term = 1

# end of class ventPrincipal

class MyApp(wx.App):
    def OnInit(self):
        self.frame = ventPrincipal(None, wx.ID_ANY, "")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

# end of class MyApp

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
