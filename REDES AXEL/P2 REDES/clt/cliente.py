#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import wx, socket, sys, base64, glob, os, pickle
sys.tracebacklimit = 0  #Menos salida de depuración
etiquetasTxt = []       #Identificadores globales de cada etiqueta
catalogo = []           #Catálogo global
SALDO_glob=1000         #Saldo (global) que posee el cliente
inicializacion = False  #Flag para verificar si se han obtenido los datos
inic_listaCompras = False #Flag para verificar si el cliente ha comprado algo
pos = 0                 #Posición inicial en el catalogo
itemsModificables = []       #Cantidad de ítems que tiene el cliente
HOST = "localhost"
PORT = 52222

def recvall(sock,unpickle=None):        # Funcion que se encarga de recibir datos a traves de un socket sin importar su tamaño
    BUFF_SIZE = 512    # Chunks de 512B
    data = b''          # data = arreglo de bytes
    while (True):           # loop infinito
        part = sock.recv(BUFF_SIZE) #recibir de 16KB en 16KB
        data += part                #anexar los datos
        if (len(part) < BUFF_SIZE): #Terminar cuando sea 0 o ya no hayan archivos
            break                       #-->break
    if(unpickle):
        return pickle.loads(data)
    else:
        return data
def actualizarCambios():    #Esta función obtiene potenciales cambios que puedan ocurrir en caso de que hayan clientes concurrentes
    global itemsModificables    #acceder para modificar
    if(BD_Datos("db_local")):
        for i in catalogo:          #El catálogo siempre va estar actualizado con los datos del servidor
            for j in itemsModificables: #Es necesario actualizar nuestro "inventario" local
                if(i["ID"]==j["ID"]):   #Actualizar cada ID con su igual
                    j["Existencia"]=i["Existencia"] #Actualizar existencias
    else:
        pass

def actualizarItemsCliente(cID):    #Función que copiará los datos de la bd relevantes a otro arreglo (id, existentes y obtenidos)
    global itemsModificables    #Modificables son los artículos del cliente y en existencia.
    if(cID<0):              #Si cID<0, se inicializa una lista con entradas para cada ítem
        datillos = {}       #...en la cual, se almacena la cantidad de cosas que ha comprado
        for i in range(len(catalogo)):      #...el cliente.
            aaa = catalogo[i]
            datillos.update({'ID':aaa["ID"], 'Obtenidos':0, 'Existencia':aaa["Existencia"]})
            itemsModificables.append(datillos.copy())
    else:
        cID+=1      #Los índices (ID) empiezan en 0, aquí se suma 1
        for i in itemsModificables:
            if(i["ID"]==cID):
                i["Obtenidos"]+=1

def modificarExistencia(cID):
    global itemsModificables
    cID+=1
    for i in itemsModificables:
        if(i["ID"]==cID):
            i["Existencia"]-=1

def modPos(posDeseada):
    global pos          #Para acceder a la variable global
    pos = posDeseada    #Actualizar valor

def BD_Datos(comando):         #Esta funcion obtiene la base de datos remota
    s = socket.socket()
    try:
        s.connect((HOST, PORT))
    except:
        return False    #En caso de fallo, regresar false
    if(comando=="db_local"):
        s.send(("db_local").encode())   #Solicitar bd
        global catalogo                 #Acceder al catalogo global
        catalogo = recvall(s,1)         #El servidor enviará la lista serializada de vuelta, aquí se guarda ya des-serializada
        s.shutdown(socket.SHUT_WR)              #anunciar cierre de conexiones al socket
        s.close()                               #cerrar socket
        for i in range(len(catalogo)):
            aaa = catalogo[i]
            with open(aaa["ArchImg"],'wb') as f:    #ArchImg es el nombre
                f.write(base64.b64decode(aaa["ArchImg64"])) #Img en B64
            f.close()
        return True
    elif(isinstance(comando, int)):
        s.send((str(pos)).encode())

class mainWindow(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: mainWindow.__init__
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.SetSize((432, 768))
        self.window_1 = wx.SplitterWindow(self, wx.ID_ANY)
        self.window_1_pane_1 = wx.Panel(self.window_1, wx.ID_ANY)
        self.window_1_pane_2 = wx.Panel(self.window_1, wx.ID_ANY)
        self.button_4 = wx.Button(self.window_1_pane_2, wx.ID_ANY, "Comprar")
        self.button_5 = wx.Button(self.window_1_pane_2, wx.ID_ANY, "Actualizar")
        self.button_6 = wx.Button(self.window_1_pane_2, wx.ID_ANY, "<<")
        self.button_7 = wx.Button(self.window_1_pane_2, wx.ID_ANY, ">>")
        self.PhotoMaxSize = 320
        self.__set_properties()
        self.__do_layout()
        self.Bind(wx.EVT_BUTTON, self.OnBtnComprar, self.button_4)
        self.Bind(wx.EVT_BUTTON, self.OnBtnActualizar, self.button_5)
        self.Bind(wx.EVT_BUTTON, self.OnBtnPrev, self.button_6)
        self.Bind(wx.EVT_BUTTON, self.OnBtnSig, self.button_7)
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: mainWindow.__set_properties
        self.SetTitle("Compras en línea (ID:"+str(os.getpid())+")")
        self.window_1_pane_1.SetMinSize((400, 300))
        self.window_1.SetMinimumPaneSize(20)
        # end wxGlade

    def __do_layout(self):
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_1 = wx.GridSizer(0, 4, 0, 0)
        sizer_3 = wx.BoxSizer(wx.VERTICAL)
        sizer_4 = wx.BoxSizer(wx.HORIZONTAL)
        img = wx.EmptyImage(300,300)
        self.imageCtrl = wx.StaticBitmap(self.window_1_pane_1, wx.ID_ANY, 
                                         wx.BitmapFromImage(img)) 
        instructLbl = wx.StaticText(self.window_1_pane_1, label="Imagen del artículo: ")
        self.photoTxt = wx.TextCtrl(self.window_1_pane_1, size=(0,0))
        #browseBtn = wx.Button(self.window_1_pane_1, label='Browse')
        #browseBtn.Bind(wx.EVT_BUTTON, self.onBrowse)
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        sizer_3.Add(wx.StaticLine(self.window_1_pane_1, wx.ID_ANY),
                           0, wx.TOP|wx.EXPAND, 5)
        sizer_3.Add(instructLbl, 0, wx.ALL, 5)
        sizer_3.Add(self.imageCtrl, 0, wx.ALL, 5)
        #sizer_4.Add(self.photoTxt, 0, wx.ALL, 5)
        #sizer_4.Add(browseBtn, 0, wx.ALL, 5)        
        sizer_3.Add(sizer_4, 0, wx.ALL, 5)
        self.window_1_pane_1.SetSizer(sizer_3)
        #end image viewer
        grid_sizer_1.Add(self.button_4, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.TOP, 4)
        grid_sizer_1.Add(self.button_5, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.TOP, 4)
        grid_sizer_1.Add(self.button_6, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.TOP, 4)
        grid_sizer_1.Add(self.button_7, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.TOP, 4)
        sizer_2.Add(grid_sizer_1, 1, wx.EXPAND, 0)
        label_20 = wx.StaticText(self.window_1_pane_2, wx.ID_ANY, "\n")
        sizer_2.Add(label_20, 0, 0, 0)
        label_13 = wx.StaticText(self.window_1_pane_2, wx.ID_ANY, "ID:\n")
        sizer_2.Add(label_13, 0, 0, 0)
        label_14 = wx.StaticText(self.window_1_pane_2, wx.ID_ANY, "Nombre:\n")
        sizer_2.Add(label_14, 0, 0, 0)
        label_15 = wx.StaticText(self.window_1_pane_2, wx.ID_ANY, u"Descripci\u00f3n:\n")
        sizer_2.Add(label_15, 0, 0, 0)
        label_16 = wx.StaticText(self.window_1_pane_2, wx.ID_ANY, "Precio:\n")
        sizer_2.Add(label_16, 0, 0, 0)
        label_17 = wx.StaticText(self.window_1_pane_2, wx.ID_ANY, "Existencia:\n")
        sizer_2.Add(label_17, 0, 0, 0)
        label_18 = wx.StaticText(self.window_1_pane_2, wx.ID_ANY, u"Promoci\u00f3n:\n")
        sizer_2.Add(label_18, 0, 0, 0)
        label_19 = wx.StaticText(self.window_1_pane_2, wx.ID_ANY, "Usted posee: \n")
        sizer_2.Add(label_19, 0, 0, 0)
        self.window_1_pane_2.SetSizer(sizer_2)
        self.window_1.SplitHorizontally(self.window_1_pane_1, self.window_1_pane_2)
        sizer_1.Add(self.window_1, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)  #Divisor
        self.Layout()   #Lanzar layout
        self.Centre()   #Centrar

    def onView(self):
        filepath = self.photoTxt.GetValue()
        img = wx.Image(filepath, wx.BITMAP_TYPE_ANY)
        # scale the image, preserving the aspect ratio
        W = img.GetWidth()
        H = img.GetHeight()
        if W > H:
            NewW = self.PhotoMaxSize
            NewH = self.PhotoMaxSize * H / W
        else:
            NewH = self.PhotoMaxSize
            NewW = self.PhotoMaxSize * W / H
        img = img.Scale(NewW,NewH)
        self.imageCtrl.SetBitmap(wx.BitmapFromImage(img))

    def OnBtnComprar(self, event):  # wxGlade: mainWindow.<event_handler>
        global SALDO_glob
        if(inicializacion):
            actualizarCambios()
            aaa = catalogo[pos]
            bbb = itemsModificables[pos]
            precioItem = int(aaa["Precio"].replace("$",""))
            enExistItem = bbb["Existencia"]
            if((SALDO_glob>=precioItem) and (enExistItem>0)):
                actualizarItemsCliente(pos)     #Suma uno a los que posee el cliente
                modificarExistencia(pos)        #Resta uno a los que están en existencia
                BD_Datos(pos)
                SALDO_glob -= precioItem
                etiquetasTxt[0].SetLabel("\n[$" + str(SALDO_glob) + " disponibles]" + " [" + str(len(catalogo)) + " ítems en la tienda] [PAGO CORRECTO]")                
                etiquetasTxt[5].SetLabel("Existencia: "+str(bbb["Existencia"])+"\n")
                etiquetasTxt[7].SetLabel("Usted posee: "+str(bbb["Obtenidos"])+" elemento(s)"+"\n")
            elif (enExistItem<1):
                etiquetasTxt[0].SetLabel("\n[$" + str(SALDO_glob) + " disponibles]" + " [" + str(len(catalogo)) + " ítems en la tienda] [ARTÍCULO AGOTADO]")
            elif (precioItem > SALDO_glob):
                etiquetasTxt[0].SetLabel("\n[$" + str(SALDO_glob) + " disponibles]" + " [" + str(len(catalogo)) + " ítems en la tienda] [SALDO INSUFICIENTE]")
        else:
            print("[E] Haga click en inicializar")

    def OnBtnActualizar(self, event):  # wxGlade: mainWindow.<event_handler>
        global inic_listaCompras
        global etiquetasTxt
        etiquetasTxt = [widget for widget in self.window_1_pane_2.GetChildren() if isinstance(widget, wx.StaticText)]
        if(BD_Datos("db_local")):
            print("[ok]")
            if(not inic_listaCompras):
                inic_listaCompras = True
                actualizarItemsCliente(-1)
            global inicializacion
            actualizarCambios()
            modPos(0)       #Actualizar el índice a la pos. 0
            inicializacion = True   #Activar flag
            aaa = catalogo[pos]   #Usar sólo lo del primer índice
            bbb = itemsModificables[pos]
            etiq0 = "\n[$" + str(SALDO_glob) + " disponibles]" + " [" + str(len(catalogo)) + " ítems en la tienda]"
            etiquetasTxt[0].SetLabel(etiq0)
            etiquetasTxt[1].SetLabel("ID: "+str(aaa["ID"])+"\n")
            etiquetasTxt[2].SetLabel("Nombre: "+aaa["Nombre"]+"\n")
            etiquetasTxt[3].SetLabel("Descripción: "+aaa["Descripción"]+"\n")
            etiquetasTxt[4].SetLabel("Precio: "+aaa["Precio"]+"\n")
            etiquetasTxt[5].SetLabel("Existencia: "+str(bbb["Existencia"])+"\n")
            etiquetasTxt[6].SetLabel("Promoción: "+aaa["Promocion"]+"\n")
            etiquetasTxt[7].SetLabel("Usted posee: "+str(bbb["Obtenidos"])+" elemento(s)"+"\n")
            self.photoTxt.SetValue(aaa["ArchImg"])
            self.onView()
        else:
            print("[E] Servidor fuera de línea...")
        
    def OnBtnPrev(self, event):  # wxGlade: mainWindow.<event_handler>
        if(inicializacion):
            if(pos>0):
                actualizarCambios()
                modPos(pos-1)
                aaa = catalogo[pos]   
                bbb = itemsModificables[pos]
                etiq0 = "\n[$" + str(SALDO_glob) + " disponibles]" + " [" + str(len(catalogo)) + " ítems en la tienda]"
                etiquetasTxt[0].SetLabel(etiq0)
                etiquetasTxt[1].SetLabel("ID: "+str(aaa["ID"])+"\n")
                etiquetasTxt[2].SetLabel("Nombre: "+aaa["Nombre"]+"\n")
                etiquetasTxt[3].SetLabel("Descripción: "+aaa["Descripción"]+"\n")
                etiquetasTxt[4].SetLabel("Precio: "+aaa["Precio"]+"\n")
                etiquetasTxt[5].SetLabel("Existencia: "+str(bbb["Existencia"])+"\n")
                etiquetasTxt[6].SetLabel("Promoción: "+aaa["Promocion"]+"\n")
                etiquetasTxt[7].SetLabel("Usted posee: "+str(bbb["Obtenidos"])+" elemento(s)"+"\n")
                self.photoTxt.SetValue(aaa["ArchImg"])
                self.onView()
        else:
            print("[E] Haga click en inicializar")

    def OnBtnSig(self, event):  # wxGlade: mainWindow.<event_handler>
        if(inicializacion):
            if(pos<(len(catalogo)-1)):
                actualizarCambios()
                modPos(pos+1)
                aaa = catalogo[pos]   
                bbb = itemsModificables[pos]
                etiq0 = "\n[$" + str(SALDO_glob) + " disponibles]" + " [" + str(len(catalogo)) + " ítems en la tienda]"
                etiquetasTxt[0].SetLabel(etiq0)
                etiquetasTxt[1].SetLabel("ID: "+str(aaa["ID"])+"\n")
                etiquetasTxt[2].SetLabel("Nombre: "+aaa["Nombre"]+"\n")
                etiquetasTxt[3].SetLabel("Descripción: "+aaa["Descripción"]+"\n")
                etiquetasTxt[4].SetLabel("Precio: "+aaa["Precio"]+"\n")
                etiquetasTxt[5].SetLabel("Existencia: "+str(bbb["Existencia"])+"\n")
                etiquetasTxt[6].SetLabel("Promoción: "+aaa["Promocion"]+"\n")
                etiquetasTxt[7].SetLabel("Usted posee: "+str(bbb["Obtenidos"])+" elemento(s)"+"\n")
                self.photoTxt.SetValue(aaa["ArchImg"])
                self.onView()
        else:
            print("[E] Haga click en inicializar")

# end of class mainWindow

class MyApp(wx.App):
    def OnInit(self):
        self.frame = mainWindow(None, wx.ID_ANY, "")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

# end of class MyApp

if __name__ == "__main__":
    if(len(sys.argv)>1):
        SALDO_glob = int(sys.argv[1])
    app = MyApp(0)
    app.MainLoop()
