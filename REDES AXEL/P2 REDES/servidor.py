#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import os, sys, base64, socket, glob, pickle
from multiprocessing import Process
sys.tracebacklimit = 0
listaDiccionarios = []
HOST = "localhost"
PORT = 52222
def BD_arreglo():
	datillos = {}			#Lista de python
	imgfiles = sorted(glob.glob("img*.*jpg"))	#Regex, sólo archivos img
	lista_imgs = []			#Lista vacía para la lista de imágenes
	try:
		with open("articulosServ.txt", "rt") as f:	#Leer el archivo 
			next(f)							#Omitir primer comentario
			for line in f:					#Leer línea por línea
				x = line.split(',')			#El separador es la coma
				lista_imgs.append(x)		#Pegar a lista_imgs cada elemento
	except:
		print("[E] no se encontró el archivo de artículos!")
		sys.exit(0)
	for i in lista_imgs:				#Ciclo para ver que estén los archivos
		i[6] = i[6].replace('\n','')	#...especificados en el txt
		if(str(i[6]) not in imgfiles):	#i[6] es la columna de archivos de imagen
			print("\n[W]",i[6],"no se encontró!")
			sys.exit(0)
		else:
			pass			#No hacer nada
	global listaDiccionarios	#Lista de diccionarios (global)
	for i in lista_imgs:	#Iterar sobre la lista de imágenes
		with open(i[6], "rb") as imgDesc:	#Abrir cada imagen 
			imgb64 = base64.b64encode(imgDesc.read())	#Transformar a B64 e insertar al dicc
		datillos.update({'ID':int(i[0]), 'Nombre':i[1], 'Descripción':i[2], 'Precio':i[3], 'Existencia':int(i[4]), 'Promocion':i[5], 'ArchImg':i[6], 'ArchImg64':imgb64})
		listaDiccionarios.append(datillos.copy()) #Pegar el diccionario a la lista

def PROC_Envio():	#Proceso que hace disponible una lista
	s = socket.socket()
	try:
		s.bind((HOST,PORT))
		print("[>>ok] proceso de envio corriendo en ",HOST,":",PORT,sep="")
		global listaDiccionarios
	except:
		print("[E] bind, ejecute fuser (",PORT,")",sep="")
		sys.exit(0)
	s.listen(5)	#backlog de 5
	while True:
		datos_serializados = pickle.dumps(listaDiccionarios) #Serializar la lista de datos
		try: 
			conn,addr = s.accept()
		except:
			print("[E] conn")
			s.close()
			sys.exit(0)
		while True:
			try: 
				data = conn.recv(512) #Aquí el servidor espera los datos
				comando = data.decode("utf-8")
				if(comando):	print("comando:",comando)
			except:
				print("[E] conn/peer")
			if(not data):	break
			if(comando=="db_local"):
				try: 
					bytes_enviados = conn.send(datos_serializados)
					print("[ok] enviados",(bytes_enviados/(1000**2)),"MB de datos. (BD)")
				except: 
					print("[E] Error del lado del cliente")
			elif(isinstance(int(comando), int)):
				cmd = int(comando) + 1	#El id que se envía siempre empieza en 0, sumar uno
				for i in listaDiccionarios:
					if(i["ID"]==cmd):
						i["Existencia"]=i["Existencia"]-1
						print("ID",i["ID"],"ahora tiene",i["Existencia"],"elementos.")
		conn.close()

if __name__ == "__main__":
	BD_arreglo()	#para inicializar la bd local
	PROC_Envio()	#se encarga de recibir comandos para actualizar/mandar datos