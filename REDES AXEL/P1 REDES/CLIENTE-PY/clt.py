import socket               # Para los sockets
import glob					# Operaciones con el sistema
import os,sys				# Para operaciones con archivos y codigos de salida
import zipfile              # Para operaciones con zips
import pickle				# Para serializar datos
import secrets				# Para generar tokens aleatorios
from pathlib import Path
from multiprocessing import Process	#Para multitarea
import wx							#Widgets
from wx.lib.mixins.listctrl import CheckListCtrlMixin, ListCtrlAutoWidthMixin
sys.tracebacklimit = 0
CarpetaDescargasLocales = "Descargas"

def getSize(filename):
    st = os.stat(filename)	#probar si esta el archivo
    return st.st_size		#checar tam

def recvall(sock):		# Funcion que se encarga de recibir datos a traves de un socket sin importar su tamaño
    BUFF_SIZE = 8192	# Chunks de 8KB
    data = b''			# data = arreglo de bytes
    while (True):			# loop infinito
        part = sock.recv(BUFF_SIZE)	#recibir de 8KB
        data += part				#anexar los datos
        if (len(part) < BUFF_SIZE): #Terminar cuando sea 0 o ya no hayan archivos
            break						#-->break
    return data

def getDirsize(start_path):
		total_size = 0
		for dirpath, dirnames, filenames in os.walk(start_path):
			for f in filenames:
				fp = os.path.join(dirpath, f)
				total_size += os.path.getsize(fp)
		return total_size

def getEverything(start_path):  #Funcion que guarda en una lista la ruta, tamaño y tipo de archivo/durectirio
    listachida=[]
    for root, dirs, files in os.walk(start_path):
        for file in files:
            arch_end=os.path.join(root, file)
            if(getSize(arch_end)>=1000*1000):
                listachida.append((os.path.abspath(arch_end),str(round((getSize(arch_end)/(1000*1000)),3))+" MB","Archivo"))
            else:
                listachida.append((os.path.abspath(arch_end),str(round((getSize(arch_end)/(1000)),3))+" kB","Archivo"))
        for dirx in dirs:
            dir_end=os.path.join(root, dirx)
            if(getDirsize(dir_end)>=1000*1000):
                listachida.append((os.path.abspath(dir_end),str(round((getDirsize(dir_end)/(1000*1000)),3))+" MB","Directorio"))
            else:
                listachida.append((os.path.abspath(dir_end),str(round((getDirsize(dir_end)/(1000)),3))+" kB","Directorio"))
    return listachida

def subfile(file):
	transferidos = 0
	s = socket.socket()         # crear un objeto de socket
	host = socket.gethostname() # localhost
	port = 12345                 # puerto
	try:
		s.connect((host, port))
	except:
		raise ValueError('\n[!] Servidor está offline')
		os.remove(file)
		return None
	f =	open(file,'rb')
	print("enviando ",file)
	l = f.read(1024)
	while (l):
	    print("enviando... ",round(((transferidos*100)/getSize(file)),2)," % \r", sep=' ', end='', flush=True)
	    try: 
	    	transferidos += s.send(l)
	    except:
	    	raise ValueError('\n[!] Servidor desconectado')
	    l = f.read(1024)
	f.close()
	print("[ok] enviados",(getSize(file))/(1000*1000),"MB")
	s.shutdown(socket.SHUT_WR)
	s.close()
	os.remove(file)

def loop1():					# Loop(PRINCIPAL) para recibir archivos
	s = socket.socket()         # Socket para crear archivos
	host = socket.gethostname() # localhost
	puertoarch = 44444          # Puerto para transferencia de archivos
	filename = "receivedfiles.zip" # 'Seed' para el nombre de archivo temporal
	curpath = os.getcwd() #Obtener la ruta actual donde se esta ejecutando el script
	try:
		s.bind((host, puertoarch))        # Ligar a puertoarch
		print("[ok/downloader] Corriendo en ",host,":",puertoarch,sep="")
	except:
		print("[!/downloader] Error (bind)")
		sys.exit(0)
	s.listen(5)                 #Backlog de 5 conexioness
	print("Still running...")
	while True:                 #Ciclo infinito
		nomrand = secrets.token_hex(10) + "_" + filename
		transferidos=0
		f = open(nomrand,'wb')
		print("[downloader] temp",nomrand,"creado")
		try: 
			c, addr = s.accept()     #Aceptar conexiones del cliente
		except:
			print("\n[!/downloader] Saliendo limpiamente")
			os.remove(nomrand)
			f.close()
			s.close()
			sys.exit(0)
		print("[downloader] descargando desde: ",addr)
		l = c.recv(1024)
		while (l):
			print("[downloader] descargando... ",transferidos/(1000**2)," MB\r", sep=' ', end='', flush=True)
			transferidos += f.write(l)
			try:
				l = c.recv(1024)
			except:
				print("[!/downloader] Transferencia incompleta. Saliendo limpiamente...")
				os.remove(nomrand)
				f.close()
				c.close()
				s.close()
				sys.exit(0)
		f.close()
		tempz = zipfile.ZipFile(nomrand, 'r')
		print("[downloader] transferidos",round(((getSize(nomrand))/(1000*1000)),3),"MBytes en",len(tempz.namelist()),"archivos")
		os.chdir(CarpetaDescargasLocales)	#Moverse a la carpeta de descargas
		tempz.extractall(os.getcwd())		#Extraer todos los archivos en el Directorio Actual de Trabajo (cwd)
		tempz.close()						#Cerrar del descriptor del zip
		os.chdir(curpath)					#Moverse un directorio arriba
		os.remove(nomrand)					#Eliminar el zip
		print("[OK/downloader] descarga completada satisfactoriamente")
		c.close()							#Cerrar el descriptor del socket

app = wx.App()				#app principal (widget)
frame = wx.Frame(None, wx.ID_ANY, 'Seleccione una opción') #frame principal
frame.SetSize(666,666,300,80)		#dimensiones en xy,666 no importa
frame.Center()						#centrar ventana
frame.Show(True)					#mostrar ventana

def onBDescargar(event):
	print("[llamada a onBDescargar]")
	app = wx.App()
	ex = Subirclass(None)
	ex.Show()
	app.MainLoop()

def getRemoteListOfFiles(hostx,portx):
	s = socket.socket()
	try:
		s.connect((hostx, portx))
	except:
		print("[!] Error fatal al obtener lista de archivos...")
		sys.exit(0)
	s.send(("123").encode())				#123 es el ping que se envía al servidor
	return pickle.loads(recvall(s))			#El servidor enviará la lista serializada de vuelta, aquí se Des-Serializa
	s.shutdown(socket.SHUT_WR)				#anunciar cierre de conexiones al socket
	s.close()								#cerrar el socket

def sendLocalListOfFiles(hostx,portx,listX):
	s = socket.socket()						#Abrir socket
	s.connect((hostx,portx))				#Conectarse al socket
	data_string = pickle.dumps(listX)		#Serializar listX
	s.send(data_string)						#Enviar lista ya serializada
	s.close()								#Cerrar el socket

class CheckListCtrl(wx.ListCtrl, CheckListCtrlMixin, ListCtrlAutoWidthMixin):
    def __init__(self, parent):
        wx.ListCtrl.__init__(self, parent, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        CheckListCtrlMixin.__init__(self)
        ListCtrlAutoWidthMixin.__init__(self)

class Subirclass(wx.Frame):
	def __init__(self, *args, **kw):
		super(Subirclass, self).__init__(*args, **kw)

		panel = wx.Panel(self)
		vbox = wx.BoxSizer(wx.VERTICAL)
		hbox = wx.BoxSizer(wx.HORIZONTAL)
		leftPanel = wx.Panel(panel)
		rightPanel = wx.Panel(panel)

		self.log = wx.TextCtrl(rightPanel, style=wx.TE_MULTILINE|wx.TE_READONLY)
		self.list = CheckListCtrl(rightPanel)
		self.list.InsertColumn(0, 'Archivo', width=1000)
		self.list.InsertColumn(1, 'Tamaño', width=100)
		self.list.InsertColumn(2, 'Tipo')
		idx = 0
		Archivos = getRemoteListOfFiles("127.0.0.1",50025)
		for i in Archivos:
			index = self.list.InsertItem(idx, i[0])
			self.list.SetItem(index, 1, i[1])
			self.list.SetItem(index, 2, i[2])
			idx += 1
		vbox2 = wx.BoxSizer(wx.VERTICAL)
		uptBtn = wx.Button(leftPanel, label='Actualizar lista')
		selBtn = wx.Button(leftPanel, label='  Marcar todos  ')
		desBtn = wx.Button(leftPanel, label=' Desmarcar todos')
		appBtn = wx.Button(leftPanel, label='   Descargar     ')

		self.Bind(wx.EVT_BUTTON, self.OnUpdate, id=uptBtn.GetId())
		self.Bind(wx.EVT_BUTTON, self.OnSelectAll, id=selBtn.GetId())
		self.Bind(wx.EVT_BUTTON, self.OnDeselectAll, id=desBtn.GetId())
		self.Bind(wx.EVT_BUTTON, self.OnApply, id=appBtn.GetId())
		vbox2.Add(uptBtn, 0, wx.TOP|wx.BOTTOM, 5)
		vbox2.Add(selBtn, 0, wx.TOP|wx.BOTTOM, 5)
		vbox2.Add(desBtn, 0, wx.BOTTOM, 5)
		vbox2.Add(appBtn)

		leftPanel.SetSizer(vbox2)

		vbox.Add(self.list, 4, wx.EXPAND | wx.TOP, 3)
		vbox.Add((-1, 10))
		vbox.Add(self.log, 1, wx.EXPAND)
		vbox.Add((-1, 10))

		rightPanel.SetSizer(vbox)

		hbox.Add(leftPanel, 0, wx.EXPAND | wx.RIGHT, 5)
		hbox.Add(rightPanel, 1, wx.EXPAND)
		hbox.Add((3, -1))
		panel.SetSizer(hbox)

		self.SetTitle('Archivos remotos')
		self.SetSize(666,666,1350,400)		#dimensiones en xy,666 no importa
		self.Centre()
	def OnUpdate(self, event): 
		Archivos = getRemoteListOfFiles("127.0.0.1",50025)
		self.list.DeleteAllItems()			#Limpiar la lista
		idx = 0								#Inicializar indice
		for i in Archivos:					#Agregar items a la lista
			index = self.list.InsertItem(idx, i[0])	#Columna del nombre
			self.list.SetItem(index, 1, i[1])		#Columna del tamaño
			self.list.SetItem(index, 2, i[2])		#Columna del tipo
			idx += 1
	def OnSelectAll(self, event):
		num = self.list.GetItemCount()
		for i in range(num):
			self.list.CheckItem(i)
	def OnDeselectAll(self, event):
		num = self.list.GetItemCount()
		for i in range(num):
			self.list.CheckItem(i, False)
	def OnApply(self, event):
		ArchivosSeleccionados=[]		#Lista vacia 
		num = self.list.GetItemCount()	#Cantidad de archivos seleccionados
		for i in range(num):
			if i == 0: self.log.Clear()	
			if self.list.IsChecked(i):
				self.log.AppendText(self.list.GetItemText(i) + '\n')
				ArchivosSeleccionados.append(self.list.GetItemText(i))	#Se guarda en la lista los archivos seleccionados
		sendLocalListOfFiles("127.0.0.1",50026,ArchivosSeleccionados)	#Llamar a la funcion para enviar los nombres de
																		#los archivos que se quieren descargar

def onBSubir(event):		#dialogo para subir cosas
	print("[llamada a onBSubir]")	
	tmpname = "CltTMPFile.zip"
	tempz = zipfile.ZipFile(tmpname,"w") #zip a enviar
	home = os.path.expanduser('~')+"/" #dir del usuario-UNIX!
	cont=0
	#para abrir archivos 
	openFileDialog = wx.FileDialog(frame, "Abrir archivo", home, "", "Hacer click en cancelar|*.*",wx.FD_OPEN | wx.FD_FILE_MUST_EXIST | wx.FD_MULTIPLE) 
	openFileDialog.ShowModal() #mostrar
	listaArchDir = openFileDialog.GetPaths()	#lista que contiene todo
	for i in listaArchDir:						#iterar sobre todo en la lista			
		if(os.path.isfile(i)):					#si es archivo, agregar 
			cont=cont+1
			filePath=os.path.relpath(os.path.join(home, i), ".")
			print("[ok:",cont,"]",filePath)
			tempz.write(filePath)							
		else:
			for root, dirs, files in os.walk(i): #explorar archivos dentro de cada carpeta
				for f in files:			
					cont=cont+1
					xFile = os.path.join(root, f) #agregar cada archivo encontrado con su ruta
					filePath=os.path.relpath(os.path.join(home, xFile), ".")
					print("[ok:",cont,"]",filePath)
					tempz.write(filePath)
	tempz.close()
	subfile(tmpname)			#subir el archivo
	openFileDialog.Destroy()	#al cerrar

for f in glob.glob("*_receivedfiles.zip"):  #Remover archivos remanentes que terminen en la regex
	os.remove(f)					
try: 
	os.remove("CltTMPFile.zip")
except:
	pass
try:                                        #En esta parte, se crea la carpeta del servidor donde se guardan las cosas
    os.mkdir(CarpetaDescargasLocales)
    print("[inf] Carpeta de archivos creada")
except:
    print("[inf] Usando carpeta de descargas existente") 

Process(target=loop1).start()	#Iniciar servicio de recepcion de archivos
panel = wx.Panel(frame, wx.ID_ANY)
subir = wx.Button(panel, wx.ID_ANY, 'Subir archivos', (10, 10))			#Boton para subir
descargar = wx.Button(panel, wx.ID_ANY, 'Descargar archivos', (140, 10))	#Boton para descargar archivos
subir.Bind(wx.EVT_BUTTON, onBSubir)			#ligar a funcion
descargar.Bind(wx.EVT_BUTTON, onBDescargar)	#ligar a funcion
app.MainLoop()								#frame principal
