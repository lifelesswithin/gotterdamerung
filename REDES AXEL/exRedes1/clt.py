#!/usr/bin/python3
import random,sys,socket,os,timeit,datetime,jsonpickle,time
from random import choice
from prettytable import PrettyTable
from operator import itemgetter
MAX = 20
HOST = "localhost"
PORT = 3000
FILLER = "_"
HACKS = False	#Cheats
SELECTED_WORDS = 2		#Palabras seleccionadas del arreglo
INVERSE = False
HACKS_STR = []

def recvall(sock,deSerialize=None):			#Función que se encarga de recibir datos
    BUFF_SIZE = 1024    # Chunks de 1KB
    data = b''          # data = arreglo de bytes
    while (True):           # loop infinito
        part = sock.recv(BUFF_SIZE) #recibir de 1KB en 1KB
        data += part                #anexar los datos
        if (len(part) < BUFF_SIZE): #Terminar cuando sea 0 o ya no hayan archivos
            break                       #-->break
    if(deSerialize):
        return jsonpickle.decode(data)	#DES-SERIALIZAR LOS DATOS QUE ESTÁN EN JSON
    else: 
        return data						#NO REALIZAR DES-SERIALIZACIÓN (DATOS CRUDOS).

def serverProbe():			#Esta función verifica que el servidor esté vivo, y nada más. 
	for i in range(0,5):
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:	
			s.connect((HOST, PORT))
			print("[net]: ",HOST,":",PORT," testing network... ",i+1,sep="",end="\r")
			s.shutdown(socket.SHUT_WR)
			s.close()
			time.sleep(.15)
		except:	err(True)
	
def startTimer():			#Esta función inicia el temporizador, una vez que se haya empezado una partida
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:	s.connect((HOST, PORT))
	except:	err(True)
	command = jsonpickle.encode(["start"])		#Comando: start (el server lo interpreta)
	s.send(command.encode())
	s.shutdown(socket.SHUT_WR)
	data = recvall(s) 
	#print("SRV-->",data.decode("utf-8"),"",sep="")
	s.close()

def DisplayLeaderBoard():	#Para mostrar la tabla de puntuaciones.
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:	s.connect((HOST, PORT))
	except:	err(True)
	command = jsonpickle.encode(["leaderboard"])	#Comando: start (el server lo interpreta)
	s.send(command.encode())						#Codificar el comando a flujo de bytes
	s.shutdown(socket.SHUT_WR)						#Anunciar fin de envío de datos
	data = recvall(s,True) 							#Recibir la T.Punt y des-serializarla
	s.close()										#Cerrar socket
	os.system("clear")								#
	tablachida = PrettyTable()						#Tablas bonitas
	tablachida.field_names = ["NOMBRE","MODO DE JUEGO","DIFICULTAD","TIEMPO (s)"]
	trueLeaderBoard = sorted(data, key=itemgetter(3))	#Ordenar la tabla de puntuaciones por tiempo
	for i in trueLeaderBoard:
		tablachida.add_row(i)						#Agregar a la tabla
	width = os.get_terminal_size().columns
	print("==> LEADERBOARD <==".center(width))
	print(tablachida)
	input("\nPresione cualquier tecla para continuar.\n")
	os.system("clear")
	#print("SRV-->",data.decode("utf-8"),"",sep="")
	

def endTimer(name,gameMode,difflv):		#Esta función se encarga de terminar un juego y agregarlo a la T.Punt. 
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:	s.connect((HOST, PORT))
	except: err(True)
	command = jsonpickle.encode(["end",name,gameMode,difflv,])		#Comando: start (el server lo interpreta)
	s.send(command.encode())
	s.shutdown(socket.SHUT_WR)
	data = recvall(s) 
	s.close()
	return round(float(data.decode("utf-8")),2)		#Por si se necesitara, regresa el tiempo que duró la partida.

def getConceptFromServer():							#PARA OBTENER SOLAMENTE EL CONCEPTO DEL SERVIDOR
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:	s.connect((HOST, PORT))
	except:	err(True)
	command = jsonpickle.encode(["concept"])		#Comando: reqlst (el server lo interpreta)
	s.send(command.encode())
	s.shutdown(socket.SHUT_WR)
	data = recvall(s)
	data = data.decode("utf-8")
	data = data.strip("\n")
	s.close()
	return data 		#True, para tener los datos ya des-serializados

def getListFromServer():
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:	s.connect((HOST, PORT))
	except:	err(True)
	command = jsonpickle.encode(["reqlst"])		#Comando: reqlst (el server lo interpreta)
	s.send(command.encode())
	s.shutdown(socket.SHUT_WR)
	data = recvall(s,True)
	s.close()
	return data 		#True, para tener los datos ya des-serializados

def randomFill(ws):
	temp = createWS()
	LETTERS="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	for row in range(0,MAX):
		for col in range(0,MAX):
			if ws[row][col]==FILLER:
				temp[row][col] = random.choice(LETTERS)
			elif(ws[row][col]!=FILLER):
				temp[row][col] = ws[row][col]
	return temp

def displayWordsearch(wss):	
	print("")
	for i in range(0,MAX+2):
		print("_ ",sep="",end="")
	print("\n")
	for row in range(0,MAX):
		line="| "
		for col in range(0,MAX):
			line = line + wss[row][col] + " "
		line = line + "|"
		print(line)
	for i in range(0,MAX+2):
		print("_ ",sep="",end="")
	print("\n")
    
def addWord(word,wordsearch):	#Agregar horizontalmente
	if(INVERSE):	reversed=[1,-1]
	else:		reversed=[1,1]
	fns = [VaddWord,HaddWord,DaddWord,DaddWord_inv]
	choice(fns)(word[::choice(reversed)],wordsearch)


def VaddWord(word,wordsearch):	#Agregar verticalmente
	if(len(word)<MAX):
		col=random.randint(0,MAX-1)	#Columna chida
		row=random.randint(0,MAX-1)	#row(0)
		if((MAX-row)<len(word)):
			while(True):
				row=random.randint(0,MAX-1)
				if((MAX-row)>len(word)):
					break
		for i in range(0,len(word)):
			if(wordsearch[row+i][col] not in [FILLER,word[i]]):
				addWord(word,wordsearch)
				break
		else:
			if(HACKS):	
				global HACKS_STR
				HACKS_STR.append("[| " + word + "]")
			for i in range(0,len(word)):
				wordsearch[row+i][col]=word[i]
	else:
		print("Error: ",word," ",MAX,"<",len(word),sep="")		

def HaddWord(word,wordsearch):	#Agregar verticalmente
	if(len(word)<MAX):
		col=random.randint(0,MAX-1)	#columnas vert
		row=random.randint(0,MAX-1)	#filas vert
		if((MAX-col)<len(word)):	
			while(True):
				col=random.randint(0,MAX-1)
				if((MAX-col)>len(word)):
					break
		for i in range(0,len(word)):
			if(wordsearch[row][col+i] not in [FILLER,word[i]]): #!="+"
				addWord(word,wordsearch)
				break
		else:
			if(HACKS):	
				global HACKS_STR
				HACKS_STR.append("[— " + word +"]")
			for i in range(0,len(word)):
				wordsearch[row][col+i]=word[i]
	else:
		print("Error: ",word," ",MAX,"<",len(word),sep="")		

def DaddWord(word,wordsearch):	#Agregar diagonalmente (izq a der)
	if(len(word)<MAX):
		col=random.randint(0,MAX-1)	#columnas vert
		row=random.randint(0,MAX-1)	#filas vert
		if((MAX-col)<len(word)):	
			while(True):
				col=random.randint(0,MAX-1)
				if((MAX-col)>len(word)):
					break
		if((MAX-row)<len(word)):
			while(True):
				row=random.randint(0,MAX-1)
				if((MAX-row)>len(word)):
					break
		for i in range(0,len(word)):
			if(wordsearch[row+i][col+i] not in [FILLER,word[i]]): #!="+"
				addWord(word,wordsearch)
				break
		else:
			if(HACKS):	
				global HACKS_STR
				HACKS_STR.append("[\ " + word +"]")
			for i in range(0,len(word)):
				wordsearch[row+i][col+i]=word[i]
	else:
		print("Error: ",word," ",MAX,"<",len(word),sep="")		

def DaddWord_inv(word,wordsearch):	#Agregar diagonalmente (der a izq)
	if(len(word)<MAX):
		col=random.randint(0,MAX-1)	#columnas vert
		row=random.randint(0,MAX-1)	#filas vert
		if(col<len(word)):	#checar que no esté hasta la izquierda
			while(True):
				col=random.randint(0,MAX-1)
				if(col>len(word)):
					break
		if((MAX-row)<len(word)):	#Checar que no esté tan arriba (MISMO)
			while(True):
				row=random.randint(0,MAX-1)
				if((MAX-row)>len(word)):
					break
		for i in range(0,len(word)):
			if(wordsearch[col-i][row+i] not in [FILLER,word[i]]): #!="+"
				addWord(word,wordsearch)
				break
		else:
			if(col<len(word)):
				addWord(word,wordsearch)
			else:
				if(HACKS):	
					global HACKS_STR
					HACKS_STR.append("[// " + word + "]")
				for i in range(0,len(word)):
					wordsearch[col-i][row+i]=word[i]
	else:
		print("Error: ",word," ",MAX,"<",len(word),sep="")		

def err(msg=None):
	print("Saliendo...") if(not msg) else print(HOST,PORT," (El servidor no responde. Saliendo...)")
	sys.exit(0)

def createWS():
	wordsearch = []
	for row in range(0,MAX):
		wordsearch.append([])
		for col in range(0,MAX):
			wordsearch[row].append(FILLER)	
	return wordsearch

def thegaMe(demonList,concept,anag=None,dif=None,name=None):
	startTimer()			#Iniciar temporizador del lado del server
	global HACKS_STR		#Para modificar la línea de cheats que se muestran
	HACKS_STR = []			#limpiar esa línea
	if(anag):				#si es en modo anagrama, que se hagan anagramas de la lista
		anagramLst = anagramList(demonList)
		wordsearch = randomClearWS(anagramLst)	#CREAR SOPA DE LETRAS RESUELTA CON ANAGRAMAS
	else:
		wordsearch = randomClearWS(demonList)	#CREAR SOPA DE LETRAS RESUELTA CON PALABRAS EN LIMPIO
	guessDemonList = []						#arreglo para la lista que el usuario vaya adivinando
	fixedWS = randomFill(wordsearch)		#sopa de letras con la sopa y letras (aleatorias)
	total=len(demonList)					#cantidad total de elementos
	start_time = timeit.default_timer()		#temporizador
	if(dif==2):								#para el modo intermedio
		arrWords = []						#crear una lista vacía
		for demon in demonList:				#por cada elemento en la lista, se crearán los sufijos de esta
			for i in range(0,len(demon)):	#ej. SATAN, SATA, SAT, SA, S
				arrWords.append(demon[:i])	#pegarlas a esa lista
		arrWords = sorted(list(filter(None, arrWords))) #ordenarlas
	while(True):
		os.system("clear")
		if(not anag):
			if(dif==3):						#modo super dificil, sólo mostrar la longitud de las palabras
				print("\nConcepto: ",concept,". (Modo difícil). Hay ",len(demonList),concept,"(s)")
				for demon,count in zip(demonList,range(0,len(demonList))):
					print("[",concept,": ",count+1,". Longitud: ",len(demon),"] ",sep="",end="")
			if(dif==2):						#modo normal, mostrar palabras a medida que se desbloqueen
				print("\nConcepto: ",concept,". (Modo intermedio). Hay ",len(demonList),concept,"(s)")
				print("A medida que vaya encontrando palabras, se le dirá si va bien o no.")
			if(dif==1):						#modo fácil, mostrar las palabras aún si no se han encontrado
				print("\nConcepto: ",concept,". (Modo fácil). Hay ",len(demonList),concept,"(s)")
				print(demonList)
		else:
			print("\nModo: Anagramas: Tendrá que ordenar los anagramas y encontrar el ",concept,"correcto.")
			print(anagramLst)
		if(HACKS):	print("\n",HACKS_STR)	#CHEATS (MUESTRAN EL TIPO DE POSICIONAMIENTO: -, /,\,|)
		displayWordsearch(fixedWS)			#MOSTRAR SOPA DE LETRAS CON LETRAS ALEATORIAS
		lbll = "Ingrese el nombre de algún " + concept + ":\t"
		guessDemon = input(lbll)
		if guessDemon in demonList:
			if(guessDemon not in guessDemonList):		#SI NUNCA LO HABÍA ENCONTRADO
				total-=1
				guessDemonList.append(guessDemon)
				print("(",total," restantes)",end="\t",sep="")
				print("OK! Encontrados hasta ahora:",sorted(guessDemonList),end="\t")
			elif(guessDemon in guessDemonList):			#SI YA LO HABÍA ENCONTRADO
				print("(",total," restantes)",end="\t",sep="")
				print("Ya ha encontrado este",concept,"! Encontrados hasta ahora:",sorted(guessDemonList),end="\t")
		else:
			if(dif==2):
				for i in demonList:
					if(i.startswith(guessDemon) and guessDemon):
						print(guessDemon,"No se encontró, pero va bien. Una ayudita:",i[:len(guessDemon)+1])
				if(guessDemon not in arrWords):	print("No se encontró el ",concept,". Restan: ",total,end="\n",sep="")
			elif(dif!=2):	print("\nNo se encontró el ",concept,".Restan: ",total,end="\n",sep="")
		if(total==0):
			os.system("clear") 
			if(anag):	endTimer(name,"Anagrama","Normal")
			elif(dif==2):	endTimer(name,"Conceptos","Intermedia")
			elif(dif==3):	endTimer(name,"Conceptos","Dificil")
			else: 			endTimer(name,"Conceptos","Facil")
			print("\t\nHA GANADO! Lista original:",anagramLst) if(anag) else print("\t\nHA GANADO!")
			displayWordsearch(wordsearch)	#MOSTRAR SOPA DE LETRAS ORIGINAL
			elapsed = timeit.default_timer() - start_time
			print("Le tomó: ",round(elapsed,1)," S acabar la sopa de letras. Presione enter para continuar.\n",sep="")
			input()
			break
		if(input("\nPresione enter para continuar, o 'salir' para acabar \t")=="salir"):
			print("\nRIP! Sólo encontró",len(guessDemonList),"de",len(demonList),concept,"(s)")
			elapsed = timeit.default_timer() - start_time
			print("Le tomó: ",round(elapsed,1)," S intentar resolver la sopa de letras\n",sep="")
			input()
			break
		#os.system("clear")

def anagramList(demonList):		# Convierte una lista a una lista con los anagramas de la misma
	anagramLst = []
	for demon in demonList:					#Cargar demonios
			anagramLst.append(''.join(random.sample(demon,len(demon))))
	return 	anagramLst

def randomClearWS(demonList):	#Crea una sopa de letras con la lista que se le otorgue, y la devuelve
	wordsearch = createWS()
	for demon in demonList:					#Cargar demonios
		addWord(demon,wordsearch)		#Agregar nombres a la lista
	return wordsearch

def initializeGame(gameMode=None,difflv=None,name=None):
	#concept = "Demonio"
	#demonList = ['AMON', 'ASMODAY','ASTAROTH' , 'AZMODEUS', 'BARBATOS', 'BASSAGO', 
	#		'BEELZEBUB', 'BELETB', 'BELIAL', 'BERITH', 'BOTIS', 'BUER', 'CAMIO', 
	#		'CROCELL', 'ELIJOS', 'PURSON', 'SAMAEL', 'SITRI', 'VALEFOR', 'ZEPAR']
	concept = getConceptFromServer()
	demonList = getListFromServer()
	if(gameMode==1):														#CONCEPTO
		thegaMe(random.sample(demonList,SELECTED_WORDS),concept,None,difflv,name)
	if(gameMode==2):														#ANAGRAMA
		thegaMe(random.sample(demonList,SELECTED_WORDS),concept,True,None,name)
		
def main():
	serverProbe()			#Checar si el servidor está vivo
	os.system("clear")
	NAME = input("Introduzca su nombre (o enter para salir): ")
	if(NAME is "" or NAME is None): err()
	MODE = input("Ingrese modo ([1] concepto, [2] anagrama, [3] leaderboard), o  enter para salir: ")
	if(MODE not in ["1","2","3"] or not MODE):		err()
	elif(int(MODE)==2):	initializeGame(2,None,NAME) 
	elif(int(MODE)==3):	DisplayLeaderBoard()
	else:
		DIFF_LV = input("Ingrese dificultad (1 fácil, 2 intermedio, 3 difícil): ")
		if(DIFF_LV not in ["1","2","3"] or not DIFF_LV):
			err()
		initializeGame(int(MODE),int(DIFF_LV),NAME)
	main()

if __name__ == '__main__':
	SELECTED_WORDS = 2		#Palabras a seleccionar de la lista
	HACKS = False			#HACKS (palabras, posición, etc...)
	INVERSE = True			#Invertir aleatoriamente las palabras
	if(len(sys.argv)>1):
		HOST = sys.argv[1]
	try:
		main()
	except KeyboardInterrupt:
		print("\nKeyboard Interrupt")
		sys.exit(0)