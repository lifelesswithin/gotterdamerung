import socket, sys, time, pickle, struct, helper
from helper import recv_data,send_data, encrypt666

def SendMessage(message,ip,public_key):
	try:	
		with socket.socket() as theSocket:
			theSocket.connect((ip,3333))
			print("MESSAGE:",message)
			data_to_be_sent = encrypt666(pickle.dumps(message),public_key)
			send_data(theSocket,message)
			theSocket.close()
			print("SOCKET CLOSED\n")
	except Exception as e:	
		print("Failure!",e)
