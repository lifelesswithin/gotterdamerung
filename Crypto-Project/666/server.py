import socket, logging, threading, datetime, copy, pickle
from helper import recv_data,send_data, get_own_ip
from time import sleep

INACTIVITY_TIME = 5 #seg
THIS_IP = "0.0.0.0"
ON_SUCCESSFULL_COMPLETION_DATA = "OK"
ON_ERROR_DATA = "ERROR"

"""
Resumen: Esta clase se encarga de iniciar una nueva instancia de un servidor 
que se encarga de recibir y enviar datos al cliente.
Cada nodo en la tabla puede durar máximo 5 minutos antes de necesitar
conectarse otra vez. (arquitectura p2p)

Parámetros:   
    <int> socket_port:    Puerto en el cual se ejecutará el servidor.

"""

class Server():      
    #
    # Inicializar el socket (TCP bloqueante con multihilo)
    #
    # Estructura de la lista de peers en este tracker:
    # [{"ip":1.2.3.4, ,"datos":DATOS.QUE.SEAN.DE.UTILIDAD, "expirationTime":s}]
    #
    # Comandos para el servidor:
    #
    # <bytes> "['Register',{literalmente lo que sea}]": Regresa la lista de peers (sin expirationTime) y actualiza
    #                           el tiempo de expiración para dicha IP.
    #
    #
    # <bytes> "['Disconnect']"  Elimina la IP que elabore la solicitud 
    #          de la lista de peers.
    #
    #
    def __init__(self,socket_port):
        self.peerlist = []
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:    self.socket.bind(("0.0.0.0",socket_port))
        except Exception as e: logging.error(str(e))
        self.socket.listen(socket_port)

        peer_list_updater = threading.Thread(target = self.__peer_list_updater)
        peer_list_updater.daemon = True
        peer_list_updater.start() 

        while True:
            client, address = self.socket.accept()   
            threading.Thread(target = self.__listen_to_client, args = (client,address)).start() #thread
    
    def __peer_list_updater(self):
        own_ip = get_own_ip()
        while True:
            logging.info(" There are "+str(len(self.peerlist))+" peer(s). Running @ "+own_ip)
            date_now = datetime.datetime.now()
            for peer in self.peerlist:
                timedelta = date_now - peer["expTime"]
                if(timedelta.seconds > INACTIVITY_TIME):    
                    logging.info(" REMOVING AFTER INACTIVITY: "+str(peer))
                    self.peerlist.remove(peer)
            sleep(INACTIVITY_TIME)

    def __send_list_of_peers(self):
        modifiedlist = copy.deepcopy(self.peerlist)
        [entry.pop("expTime",None) for entry in modifiedlist]
        return  pickle.dumps(modifiedlist)


    #Resumen: Busca el valor de una llave en un arreglo de diccionarios en una lista.
    
    def __search_in_list(self,theList,ip,field):
        return next((item for item in theList if item[field] == ip),None)

    def __listen_to_client(self, client, address):
        while True:
            try:
                data = recv_data(client)
                if data:
                    client_ip = client.getpeername()[0] 
                    if client_ip.startswith("127"):
                        client_ip = get_own_ip()
                    data = pickle.loads(data)
                    current_entry = self.__search_in_list(self.peerlist,client_ip,"ip")
                    ##### 
                    #####   Si el comando es Registrar 
                    #####
                    if data[0]=="Register":
                        if current_entry is not None:  self.peerlist.remove(current_entry)
                        current_entry = {
                            "ip":       client_ip,
                            "expTime":  datetime.datetime.now()
                        }
                        current_entry.update(data[1])
                        self.peerlist.append(current_entry)
                        send_data(client,pickle.dumps("OK"))
                
                    ##### 
                    #####   Si el comando es GetList, enviar la lista de peers
                    #####

                    elif data[0] == "GetList":
                        send_data(client,self.__send_list_of_peers())
                    ###
                    ### Si el comando es Disconnect
                    ###
                    elif data[0]=="Disconnect" and current_entry is not None:    
                        logging.info(" Removed on request: "+str(client.getpeername()))
                        self.peerlist.remove(current_entry)
                        send_data(client,pickle.dumps("OK"))
                    else:
                        send_data(client,pickle.dumps("ERROR"))
                    break
                else:  raise ValueError("Client disconnected")
            except Exception as e:
                print("ERROR:",e)
                client.close()                          
                break       




if __name__ == "__main__":	
    logging.basicConfig(level=logging.INFO)
    Server(1234)
    print('aaa')