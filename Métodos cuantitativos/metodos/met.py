#!/usr/bin/python3
import sys,random,os,math,copy
from fractions import Fraction
from operator import itemgetter
from prettytable import PrettyTable
from decimal import *
import time

def porcentaje(num,total):	return Decimal(((num*100)/total)/100)
def dec_aleatorio():	return random.randint(0,99)/100
def MaxMin(lista):	return [min(lista),max(lista)]

def calcularRangosVariables(restricciones,cantidadVariables):	#Calcula los rangos de cada variable (inferior,superior) dada una cantidad de variables
		restVals=[]
		for rest in range(cantidadVariables):
			restIndiv=[]
			for item in restricciones:	restIndiv.append(item[rest])
			restVals.append(MaxMin([elem for elem in restIndiv if elem != None]))
		return restVals

def TabularYRealizarCalculos2(xyzValores,fObjetivo,zVals,zTot,vectores,verbosidad=0):
	try:
		x = PrettyTable()		#Tabla con formato
		x.field_names = ["V#", "VECTOR(ES) ("+str(len(fObjetivo))+")", "Z", "PorcZ", "Z-Acum", "#ALEATORIO","N.Vector"]
		porcentajeTot = 0		#Para guardar el porcentaje total (sirve para asegurarse que sea 1)
		zAcum = 0				#Inicializar el valor de la Z Acumulada
		zAcumLista = []			#En esta lista guardamos el valor de Z acumulado y el número de vector que lo posee 
		zValores = []		
		nuevosVectores=[]
		datos = []				#Esta lista es la chida y contiene los datos necesarios para las siguientes iteraciones.
		try:
			for z in range(len(xyzValores)):		#Iterar sobre todos los valores de Z
				porcentajeTot += porcentaje(zVals[z],zTot)		#Es el porcentaje total
				zAcum+=porcentaje(zVals[z],zTot)				#Porcentaje acumulado
				zAcumLista.append([zAcum,z])
				zValores.append([round(zVals[z],7),z])		#Guardamos aquí el valor de Z para calcular cuál es el más alto
				datos.append([z,vectores[z],round(zVals[z],7),round(porcentaje(zVals[z],zTot),7),round(zAcum,7),dec_aleatorio(),"-"])	
			try:
				for entrada in datos:
					nv=buscarMayorCercano(entrada[5],zAcumLista)[1]	#entrada[5] es el valor aleatorio que tomamos como referencia 
					nuevosVectores.append(nv)
					entrada[6]=nv 			#entrada[6] es para el nuevo vector
					x.add_row(entrada)										#para buscar el inmediato superior en la lista de valores de ZAcumulados
			except Exception as e:	print("!!!!!!!!!!Ha ocurrido un error (TyRC-Calc1): ",e)
			x.add_row(["-","-","∑="+str(round(zTot,5)),"∑="+str(round(porcentajeTot,5)),"-","-","-"])
		except Exception as e:	print("!!!!!!!!!!Ha ocurrido un error (TyRC-Calculos): ",e)
		if(verbosidad):	print("\n=== Tabla con vectores y valores")
		print(x)
		mayor=buscarMayor(zValores)		
		print("[info] El valor más alto de Z fue: ",mayor[0],", con las variables del vector ",mayor[1],sep="")
		nv=delDupAndOrd(nuevosVectores)		#Obtener la lista de los vectores que se repiten más, función chida
		nuevosVectoresRecombinados=[]		#Nueva lista de vectores recombinados
		if(verbosidad):	print("[cambios en vectores]: ",end="")
		for a in range(len(datos)):			#En el rango de todas las entradas que contengan los datos
			if(a<len(nv)):					#mientras a sea menor a los vectores que sólo tenemos que "reasignar"
				#print(datos[a][0],"(vector",datos[a][1],")--->",nv[a],"vector(",datos[nv[a]][1],")")
				if(verbosidad):	print("[V",datos[a][0],"->",nv[a],sep="",end="] ")
				nuevosVectoresRecombinados.append(datos[nv[a]][1])		
			else:	
				vmutado = mutar(datos[a][1])
				nuevosVectoresRecombinados.append(vmutado)
				if(verbosidad):	print("[V",datos[a][0],"->☢]",sep="",end=" ")
		if(verbosidad):
			print("\n[nuevos vectores]: ",end="")
			for vec in nuevosVectoresRecombinados:	print(vec,sep="",end=" | ")
			print("\n")
		return nuevosVectoresRecombinados,xyzValores[mayor[1]]
	except Exception as e:	print("!!!!!!!!!!Ha ocurrido un error (TabularYRealizarCalculos2): ",e)

def calcularZs(xyzValores):
	print("alcanzado calcularZs")
	zTot=0			#Sumatoria de todos los valores de Z (uno por cada población)
	zVals=[]		#Valores de Z por población individual
	for z in xyzValores:	
		zTot+=z[len(z)-1]				#El z[len(z)-1] es el ÚLTIMO elemento en esta lista
		zVals.append(z[len(z)-1])		#En esta posición, siempre va estar el valor de z, en las anteriores los valores de las variables w,x,y,z,etc...
	return zTot,zVals

def realizarCalculos(restriccionesCrudas,vectores,restriccionesYvalorEspecial,funcionObjetivo,verbosidad=0):
	try:
		if(verbosidad):	print("===valores de las variables ... (x,y,z,etc...)")
		xyzValores = []	#Aquí se guarda el resultado de x,y,z,... multiplicados en la función objetivo
		for item in vectores:					#Cada lista en la lista corresponde a los valores de una población
			resultadoVarPoblacion = []
			#indice=1					#elemitem[0] corresponde a esta parte: [[X],[ignorado]]
			contx=0
			while(True):
				contx+=1
				checkvals=[]		
				for res in restriccionesCrudas:		#Las restricciones crudas son las que se meten al inicio del programa, y siempre de la forma similar a: x1+x2>0
					checkdis=[]						
					for i,elemitem,restriccion in zip(range(len(funcionObjetivo)),item,restriccionesYvalorEspecial):	#Evaluar la X que saquemos, pero respecto a la X/Y/etc... de la restricción
						Bar = (restriccion[0]+elemitem[0]*restriccion[1])*(res[i])		#Este es el valor ya evaluado
						checkdis.append(Bar)															#Meterlo a una lista con los resultados para esta restricción
						if(verbosidad):	print("|>>V=",elemitem[0],"}{(",restriccion[0],"+",elemitem[0],"*",restriccion[1],")*",res[i]," = ",round(Bar,3),"} | ",end="",sep="")	#Guardamos estos valores que son únicos para cada elemento de la población en una lista
						#print(res[i],"*",round(Decimal(restriccion[0]+elemitem[0]*restriccion[1]),3),"=",round(Bar,3),end=" ")
					if(verbosidad):	print("")
					checkvals.append(checkdis)	#Cada 'checkdis' va tener sólo UNA RESTRICCIÓN ya evaluada
				Zcompr=[]						#En esta lista, almacenamos los valores de Z de cada restricción
				for vals in checkvals:
					sumT=0
					for valorcillo in vals:	sumT+=valorcillo	#Iteramos sobre todos los valores y simplemente los sumamos
					Zcompr.append(sumT)		
				csum = 0
				for zv,rc in zip(Zcompr,restriccionesCrudas):	#zv es un valor Z específico a una restricción y rc es el valor que se debe respetar que metimos en las restricciones
					if(rc[-2]==">"):							#rc[-2] es de las "restriccionesCrudas" y este campo es el penúltimo, donde se encuentra el signo '<' o '>'
						if(zv>=rc[-1]):							#rc[-1] es el último campo donde está el valor de Z a respetar
							if(verbosidad):	print(zv,"ok =>",rc[-1])
							csum+=1								#Este contador va ir aumentando conforme cada restricción se cumpla
						else:	
							#if(verbosidad):	print(zv,"nope =>",rc[-1])
							break
					else:	
						if(zv<=rc[-1]):							#En el caso de que la restricción sea "<="
							if(verbosidad):	print(zv,"ok <=",rc[-1])
							csum+=1
						else:	
							#if(verbosidad):	print(zv,"nope <=",rc[-1])
							break
				if(csum==len(Zcompr)):	break					#Cuando se cumplan todas las condiciones, romper el ciclo y seguir adelante
				else:	
					#item=mutar(item)							#En todo caso, mutar el item que no cumple las condiciones
					item=mutarVEntero(item)						#Usar función más rápida
					print(csum,"/",len(Zcompr),"ejecutado [",contx,"]veces.",item,end="\r")

			z = 0
			for elemitem,restriccion,fobj in zip(item,restriccionesYvalorEspecial,funcionObjetivo):	#Iterar sobre cada elemento y restricción con los valores
				valVar = Decimal(restriccion[0]+elemitem[0]*restriccion[1])*Decimal(fobj)		#rest[0] es el limite inferior, elemitem[0] es el elemento numerico de la poblacion y restriccion[1]=(sup-inf)/(2^mj-1)
				resultadoVarPoblacion.append(valVar)				#Estos son los valores de x,y,z,...etc ya evaluados en la función objetivo
				if(verbosidad):	print("{V=",elemitem[0],(elemitem),"}{(",restriccion[0],"+",elemitem[0],"*",restriccion[1],")*",fobj," = ",round(valVar,4),"} | ",end="",sep="")	#Guardamos estos valores que son únicos para cada elemento de la población en una lista
			#	indice+=1											
			for numero in resultadoVarPoblacion:		#Para sacar la Z, sólo hay que sumar el resultado que ya está multiplicado por la F.O
				z+=numero
			resultadoVarPoblacion.append(z)				#Pegar la Z (hasta el final) a la lista que contiene los resultados de todas las variables (w,x,y,etc...) 
			xyzValores.append(resultadoVarPoblacion)					#que después se meterá a una lista más grande (lista de listas)
			if(verbosidad):	print("Z:",round(z,4))
		return xyzValores
	except Exception as e:	print("!!!!!!!!!!Ha ocurrido un error (realizarCalculos): ",e)

def generarVectores(Poblaciones,cantidadBits):
	vectores = []							#Esta lista va contener los vectores
	for vector in range(Poblaciones):			#Crear vectores dado el tamaño de la población
		vIndRango = []						#Lista que va contener los vectores para todas las restricciones de una sola población
		for longitud in cantidadBits:			#Es decir, si hay 2 poblaciones y 3 restricciones, la lista será de la forma
			a = crearArregloBin(longitud)	#[[[1],[2^3]],[[3],[2^3]],[[6],[2^3]]] donde en la otra lista se guarda la cantidad de bits (para mutar dentro del rango)
			vIndRango.append(a[1])		#Meterlo a la lista, pero no meter el vector en binario (no es necesario, sólo meter el valor decimal)
		vectores.append(vIndRango)		#Meter esta lista de listas correspondiente a la población	
	return vectores

def delDupAndOrd(listilla):
	lista=copy.deepcopy(listilla)
	buff = []
	rep = []
	lastItem = -1
	for item in lista:
		oc = 0
		if item not in buff:
			for item2 in lista:
				if item == item2:
					oc += 1
			rep.append(oc)
			buff.append(item)
		lastItem = item
	n = len(rep)
	for i in range(n):
		for j in range(0, n-i-1):
			if rep[j] < rep[j+1]:
				rep[j], rep[j+1] = rep[j+1], rep[j]
				buff[j], buff[j+1] = buff[j+1], buff[j]
	return buff

def buscarMayor(lista):
	return sorted(lista, key=itemgetter(0))[-1]

def buscarMayorCercano(num,lista):
	try:
		a = sorted(lista, key=itemgetter(0))
		for item in a:
			if(num>item[0]):	pass
			elif(num<item[0]):	return [round(item[0],7),item[1]]	#Regresar el elemento y el numero de vector en el que está el elemento cercano más grande
	except Exception as e:	print("!!!!!!!!!!Ha ocurrido un error (buscarMayorCercano): ",e)

def mutarVEntero(vector):
	nuevoVectTmp=[]
	nuevoVect=[]
	for vect in vector:	nuevoVectTmp.append(mutar([vect]))		
	for vect in nuevoVectTmp:
		for vecttt in vect:	nuevoVect.append(vecttt)
	return nuevoVect

def mutar(vector):
	#random.seed(int(round(time.time() * 1000)))
	nuevoVect = copy.deepcopy(vector)				#Usar deepcopy para NO alterar el arreglo original
	randIndice = random.randint(0,len(nuevoVect)-1)	#Elegir un índice aleatorio en la lista
	randVect = nuevoVect[randIndice]					#Elegir el vector que el índice haya indicado
	randSumarORestar = random.choice([0,1])	#Si es 0, se RESTA un bit entre el rango de este número, si es 1 se SUMA un bit al número
	maximo = 2**randVect[1]-1				#Obtener el máximo que es posible generar
	nv = random.choice([math.pow(2,i) for i in range(randVect[1])])	#Seleccionar una potencia de 2 aleatoriamente dentro del rango permitido, por ejemplo, 1,2,4,etc...
	if(randSumarORestar==0):		#CASO DE RESTA
		if((randVect[0]-nv)>0):		#verificar que el nuevo número aleatorio no haga negativo el nuevo vector
			nuevoVect[randIndice][0]=randVect[0]-nv 	#restar
#			print("Resta",nuevoVect)
			return nuevoVect
	else:							#CASO DE SUMA
		if((randVect[0]+nv)<maximo):	#verificar que el número creado no sobrepase el límite
			nuevoVect[randIndice][0]=randVect[0]+nv	#sumar si se cumple la condición
#			print("Suma",nuevoVect)
			return nuevoVect
	return mutar(vector) #se manda el vector entero de vuelta para asegurar verdadera aleatoriedad

def bin2dec(vector):	return int(vector[0],2)

def ZmaximaConValores(lista):
	getter = len(lista[0])
	maxim=sorted(lista, key=itemgetter(getter-1))[-1]
	print("El maximo fue:",maxim)
	valoresVariables=[]
	for i in range(len(maxim)-1):	valoresVariables.append(maxim[i]	)
	return valoresVariables,maxim[-1]

def crearArregloBin(cant):
	listaConArreglo = []
	arregloBin = ''
	randNum = bin(random.randint(0,2**cant-1))[2:]	#[2:] para quitar el '0b'
	bitsFaltantes = cant-len(randNum)				#Por si el numero binario no ocupa todos los números
	for i in range(bitsFaltantes):
		arregloBin = arregloBin + '0'				#Llenar con 0 lo que falte
	listaConArreglo.append(arregloBin+randNum)		#0 + el número en binario
	return listaConArreglo,[bin2dec(listaConArreglo),cant]	#Regresar lista o numero en decimal generado

def GeneticosChidos(restriccionesCrudas,PobNum,cantidadBits,fObjetivo,multiplicadorRestricciones,iterc=None,verbosidad=None,vectores=None,mejorValor=None):
	try:
		if(iterc>=1):
			print("RESTAN",iterc,"ITERACIONES")	#En la primera llamada, vectores=None, por lo que se generan vectores nuevos
			if(vectores==None):	
				vectores = generarVectores(PobNum,cantidadBits)		#Generar PobNum (n. de poblaciones) listas, con cada población conteniendo n cantidaddeBits IMPORTANTE
				mejorValor=[]
			else:	pass
			xyzValores = realizarCalculos(restriccionesCrudas,vectores,multiplicadorRestricciones,fObjetivo,verbosidad)		#IMPORTANTE
			zTot,zVals = calcularZs(xyzValores)
			nuevosVectores,mejval = TabularYRealizarCalculos2(xyzValores,fObjetivo,zVals,zTot,vectores,verbosidad)
			mejorValor.append(mejval)
			GeneticosChidos(restriccionesCrudas,PobNum,cantidadBits,fObjetivo,multiplicadorRestricciones,iterc-1,verbosidad,nuevosVectores,mejorValor)
		else: 
			#print("Mejor valor encontrado:",max(mejorValor))
			#print(mejorValor)
			sys.exit(0)
	except:	
		final = ZmaximaConValores(mejorValor)
		print("El mejor valor fue",final[1],"con las variables",[round(num,4) for num in final[0]],"(faltaron",iterc,"iteraciones).")

def Geneticos():
	restriccionesCrudas = []
	cantidadBits = []
	fObjetivo = []
	rangos = []	#### TODO ESTO ES ENTRADA DE DATOS 
	IterNum = int(input("Ingrese la cantidad de ITERACIONES: "))	
	BisPNum = int(input("Ingrese la cantidad de BITS DE PRECISIÓN: "))	
	PobNum = int(input("Ingrese el tamaño de la POBLACIÓN: "))	
	cantVariables = int(input("Ingrese la cantidad de VARIABLES (x1,x2,...xn): "))	#Cantidad de variables
	RestricNum = int(input("Ingrese la cantidad de RESTRICCIONES: "))
	for variable in range(cantVariables):										
		fObjetivo.append(float(input("	Ingrese el valor de la variable ["+str(variable+1)+"] de la función objetivo: ")))
	for restriccioncilla in range(RestricNum):
		listilla=[]
		print("### Valores restricción",restriccioncilla+1,"####")
		for variable in range(cantVariables):
			listilla.append(float(input("	Ingrese coeficiente de la restricción "+str(variable+1)+" (R"+str(restriccioncilla+1)+"): ")))
		tipo=input("	Menor? o mayor? (< para ≤, > para ≥): ")
		if(tipo not in ["<",">"]):		#Verificar que se meta uno de los dos caracteres solamente
			print("Introduzca '<' o '>'. (saliendo)")
			sys.exit(0)
		else: listilla.append(tipo)
		listilla.append(float(input("	Coeficiente de Z: ")))
		restriccionesCrudas.append(listilla)
	restriccionesChidas=[]	#Esta lista va contener las "restricciones en bruto", así como si es mayor o menor al valor de Z (que está hasta el final)
	for restriccion in restriccionesCrudas:	
		restriccIndividual=[]
		for valRestriccion in range((len(restriccion)-2)):	#Le quitamos 1 porque los arreglos empiezan en 0, y le quitamos otro porque en ese campo está el "<"/">"
			#print(restriccion[valRestriccion],"Z:",restriccion[len(restriccion)-1],sep=" ",end=" | ")
			if(restriccion[valRestriccion]!=0):	restriccIndividual.append((restriccion[len(restriccion)-1])/restriccion[valRestriccion])		#Verificar división por cero
			else:	restriccIndividual.append(None)																								#Si es 0, meter "None"
		if(restriccIndividual):	restriccionesChidas.append(restriccIndividual)
	restricciones = calcularRangosVariables(restriccionesChidas,cantVariables)	#Las restricciones con sólo los límites inferiores/superiores
#	"""			
	os.system("clear")
	#### FIN DE LA ENTRADA DE DATO(S)
	print("### Restricciones: ###")
	print(restriccionesCrudas,"===")
	indice = 1									#CALCULOS INICIALES QUE SÓLO SE HACEN UNA VEZ
	multiplicadorRestricciones = []
	for i in restricciones:						#Cada "i" es una restricción
		valorMedio = (i[1]-i[0])*(10**BisPNum)	#Este es el valor que está en medio, para cada restricción
		mjRestriccionActual = math.ceil(math.log(valorMedio)/math.log(2))	#Cantidad de bits por cada restricción a usar
		cantidadBits.append(mjRestriccionActual)				#Pegarlo a la lista que va definir qué tantos bits habremos de usar para c/ var
		minRestriccionActual = 2**(mjRestriccionActual-1)					#Mínimo
		maxRestriccionActual = (2**(mjRestriccionActual))-1					#Máximo
		valMulRestriccion = (i[1]-i[0])/((2**mjRestriccionActual)-1)		#Valor de mjVar(sup-inf)/(2^mj-1)
		multiplicadorRestricciones.append([i[0],valMulRestriccion])				#Pegarlo a la lista (limite inferior + valMulRestriccion)
		print(i[0]," ≤ x",indice," ≤ ",i[1]," | ",minRestriccionActual," ≤ ",valorMedio," ≤ ",maxRestriccionActual\
			," (",mjRestriccionActual," bits para x",indice,") VA:",round(valMulRestriccion,5),sep="")		#Mostrarlo al usuario ####(IMPRESIÓN)
		indice+=1															#Índice (sólo para mostrar)
	print("\n### Función objetivo: ###\n Z = ",end="")						###IMPRESIÓN
	for i in range(0,len(fObjetivo)):										###IMPRESIÓN	
		print(fObjetivo[i],"*x",i+1," ",sep="",end="")						###IMPRESIÓN
	print("")																###IMPRESIÓN
	###Generar vectores e imprimir algunas cosas
	print("\nvalores para las restricciones:",[i[1] for i in multiplicadorRestricciones])
	print("coeficientes de la función objetivo:",fObjetivo)
	print("límite inferior de cada restricción",[i[0] for i in multiplicadorRestricciones])
	a=[]
	GeneticosChidos(restriccionesCrudas,PobNum,cantidadBits,fObjetivo,multiplicadorRestricciones,IterNum,0,None,a)
#	"""
if __name__ == "__main__":
	try:
		sys.setrecursionlimit(10000)
		Geneticos()
		#a = [[2,3],[4,5]]
		#print(a,mutar(a),a)
		#print(a,mutarVEntero(a))
	except:
		sys.exit(0)